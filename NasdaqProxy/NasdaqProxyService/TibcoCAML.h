#pragma once

#include "..\CAML\include\caml.h"

namespace NasdaqProxy 
{ 
	using namespace caml;
	

	class CTibcoCAML
	{		
	public:
		CTibcoCAML(void);
		~CTibcoCAML(void);

		bool Start(char * szConfigFile);
		bool StartThrReceiver();
		bool StartSender();
		bool Send(unsigned char * buffer, int size);
		
		static void CAML_C_API TaggedMessageCallback(TaggedMessage* pMessage, void* pClosure);

		ReliableTransport *			m_pTransport;
		AsynchReliableReceiver *	m_pReceiver;
		AsynchReliableSender *		m_pSender;
		TaggedMessage *				m_pMessage;
		Topic *						m_pTopic;
	};

}