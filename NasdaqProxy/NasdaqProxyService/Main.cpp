
#include "stdafx.h"

#include <iostream>
#include ".\ServiceCore\dbg.h"
#include ".\ServiceCore\Exception.h"

#include ".\ServiceCore\Singleton.h"
#include ".\ServiceCore\NasdaqService.h"

using namespace std;
using namespace NasdaqProxy;



int _tmain(int argc, _TCHAR* argv[])
{
	try 
	{	
		WORD wVersionRequested;
		WSADATA wsaData;
		int err;

		/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
		wVersionRequested = MAKEWORD(2, 2);

		err = WSAStartup(wVersionRequested, &wsaData);
		if (err != 0) {
			/* Tell the user that we could not find a usable */
			/* Winsock DLL.                                  */
			std::cout << "WSAStartup failed with error: %d\n" << err;
			return 1;
		}

		/* Confirm that the WinSock DLL supports 2.2.*/
		/* Note that if the DLL supports versions greater    */
		/* than 2.2 in addition to 2.2, it will still return */
		/* 2.2 in wVersion since that is the version we      */
		/* requested.                                        */

		if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) {
			/* Tell the user that we could not find a usable */
			/* WinSock DLL.                                  */
			std::cout << "Could not find version 2.2 dll of Winsock.dll\n";
			WSACleanup();
			return 1;
		}   

		/* The Winsock DLL is acceptable. Proceed to use it. */

		_setmaxstdio(2048); // allow the maximum number of files in stdio
		
		CSingleton<CNasdaqService>::getInstance().Startup();		

		WSACleanup();
	}	 
	catch(CException &err)
	{	
		std::cerr << _T("Erro: ") << err.getMessage() << std::endl;
		CWinService::LogEvent(EVENTLOG_ERROR_TYPE, 0,ENVMSG_GENERIC, err.getMessage().c_str());
		return -1; 
	}
	catch(std::exception &err)
	{	
		std::cerr << _T("Erro: ") << err.what() << std::endl;
		CWinService::LogEvent(EVENTLOG_ERROR_TYPE, 0,ENVMSG_GENERIC, err.what());
		return -1; 
	}

	return 0;
}

