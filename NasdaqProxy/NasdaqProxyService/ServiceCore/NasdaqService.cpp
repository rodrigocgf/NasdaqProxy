/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: NasdaqService.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a
	Company : 7COMm / Consulting Group	
	Date	: june 9 , 2010
=========================================================

*/

#include "stdAfx.h"
#include <iostream>
#include "dbg.h"
#include "BmfWin32Utils.h"
#include "Win32Exception.h"
//#include "VersionInfo.h"

#include "NasdaqService.h"
#include "..\NasdaqParser\Logger.h"

namespace NasdaqProxy 
{

	const TCHAR * CNasdaqService::szServiceName = _T("NasdaqProxy");
	const TCHAR * CNasdaqService::szServiceDisplayName = _T("Nasdaq Proxy");
	const TCHAR * CNasdaqService::szServiceDescription = _T("Entry point to receive NASDAQ Broadcast.");
	

	CNasdaqService::CNasdaqService(void) :
		CWinService(	szServiceName, 
						szServiceDescription, 
						szServiceDisplayName, 
						&tagNTEventSourceInfo(szServiceName), 
						SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN, COINIT_APARTMENTTHREADED),
						m_hEvStopRouter(NULL),
						m_hThService(NULL)
	{
	}

	CNasdaqService::~CNasdaqService(void)
	{
		if (NULL != m_hEvStopRouter) 
		{
			CloseHandle(m_hEvStopRouter);
			m_hEvStopRouter = NULL;
		}

		if (NULL != m_hThService) 
		{
			CloseHandle(m_hThService);
			m_hThService = NULL;
		}
	}

	BOOL CNasdaqService::Startup()
	{
		

		Logger::logtrc('C',"  CNasdaqService::Startup()  ");		

		// Add your code initialization for your Service execution
		// and at the end call CWinService::Startup to finalize
		// finding executable file path
		TCHAR szDrive[_MAX_DRIVE];
		TCHAR szDir[_MAX_DIR];
		TCHAR szFname[_MAX_FNAME];
		TCHAR szExt[_MAX_EXT];
		TCHAR szModuleFilePath[MAX_PATH];
		::GetModuleFileName(NULL, szModuleFilePath, MAX_PATH);
		_tsplitpath( szModuleFilePath, szDrive, szDir, szFname, szExt );
		
		// assemble the path
		TCHAR szAppPath[MAX_PATH];
		_tcsncpy(szAppPath, szDrive, MAX_PATH-1);
		_tcsncat(szAppPath, szDir, __min( _tcslen(szDir), MAX_PATH-_tcslen(szAppPath)-1));

		// changing current directory to use relative paths
		BOOL bResult = ::SetCurrentDirectory(szAppPath);
		VERIFY_THROW_WIN32(!bResult, _T("CNasdaqService::Startup()"), ::GetLastError());

		return CWinService::Startup();
	}

	bool CNasdaqService::OnStart()
	{

		// Add your code here to respond to SCM Start command
		Logger::logtrc('C',"  CNasdaqService::Start()  ");		

		m_Config.LoadConfiguration();

		unsigned int nThId = 0;
		m_hEvStopRouter = ::CreateEvent(NULL,FALSE,FALSE,NULL);
		ASSERT(NULL != m_hEvStopRouter);
		VERIFY_THROW_WIN32((NULL == m_hEvStopRouter), _T("CNasdaqService::CRouterService()"), ::GetLastError());
		m_hThService = (HANDLE) _beginthreadex(
										NULL, 
										0,
										CNasdaqService::Execute,
										(void *) this,
										0,
										&nThId);
		ASSERT(NULL != m_hThService);
		//VERIFY_THROW_EXCEPTION(NULL == m_hThService, "CNasdaqService::CRouterService()","Could not create Service Thread", -1);
		return true;
		
	}

	void CNasdaqService::OnPause()
	{
		// Add your code here to respond to SCM Pause command
		// this service won't support the pause/continue feature
	}

	void CNasdaqService::OnContinue()
	{
		// Add your code here to respond to SCM Continue command
		// this service won't support the pause/continue feature
	}

	void CNasdaqService::OnStop()
	{
		Logger::logtrc('C',"  CNasdaqService::Stop()  ");		

		BOOL bRet = FALSE;
		ASSERT(NULL != m_hEvStopRouter);
		if (NULL != m_hEvStopRouter)
		{
			bRet = SetEvent(m_hEvStopRouter);
			ASSERT(0 != bRet);
		}
		
		const bool bTimeOut = (::WaitForSingleObject(m_hThService, /*29950*/INFINITE) == WAIT_TIMEOUT);
		ASSERT(!bTimeOut);
		if (bTimeOut)
		{
			bRet = TerminateThread(m_hThService, 1);
			ASSERT(0 != bRet);
		}
		if (NULL != m_hThService) 
		{
			CloseHandle(m_hThService);
			m_hThService = NULL;
		}
		if (NULL != m_hEvStopRouter) 
		{
			CloseHandle(m_hEvStopRouter);
			m_hEvStopRouter = NULL;
		}
	}

	BOOL CNasdaqService::OnCustomStartupCommand(LPCTSTR szCmd, LPCTSTR szExtraInfo)
	{
		BOOL bQuit = FALSE;
		return bQuit;
	}

	unsigned int __stdcall CNasdaqService::Execute(void *pvParam)
	{
		Logger::logtrc('C'," CNasdaqService::Execute ");

		CNasdaqService * p_this = (CNasdaqService *) pvParam;
		p_this->Run();	 
		
		return 0;
	}

	void CNasdaqService::Run()
	{
		Logger::logsys('C',"  ------------------------------------------   ");
		Logger::logsys('C'," /                                          \\ ");
		Logger::logsys('C',"                  START                        ");
		m_hEvStopRouter = ::CreateEvent(NULL,FALSE,FALSE,NULL);
		ASSERT(NULL != m_hEvStopRouter);

		if ( m_TibcoCaml.Start("TibcoConfigFile.xml") )
		{
			m_TibcoCaml.StartSender();
		}

		receiverPtr = ReceiverFactory::CreateReceiver(m_Config,this);
		receiverPtr->Start();

		WaitForSingleObject(m_hEvStopRouter, INFINITE);

		Logger::logsys('C',"                   END                         ");
		Logger::logsys('C'," \\                                          / ");
		Logger::logsys('C',"  ------------------------------------------   ");
	}		

	void CNasdaqService::SendToTibco(unsigned char * buffer, int size)
	{	
		//m_TibcoCaml.Send(buffer, size );


		string strTitle;// = "NUM SEQ : ";
		strTitle.append(receiverPtr->GetLastSequenceNumber());

		Logger::logtrc('D',(char *)strTitle.c_str() );
		//Logger::logtext('D',(char *)strTitle.c_str(), buffer, size);				

		
	}
}