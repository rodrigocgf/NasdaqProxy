

#ifndef SINGLETONSTDCREATIONPOLICY_E601A50D_7537_44CE_AD76_87AAA5640B1E___INCLUDED_
#define SINGLETONSTDCREATIONPOLICY_E601A50D_7537_44CE_AD76_87AAA5640B1E___INCLUDED_

namespace NasdaqProxy 
{

	template<typename T>
	class CSingletonStdCreationPolicy
	{
	protected:
		// static members to override, if desired
		inline static T* CreateInstance() { return new T(); }
		inline static void DestroyInstance(T *p) { delete p; }
	};

} 


#endif // SINGLETONSTDCREATIONPOLICY_E601A50D_7537_44CE_AD76_87AAA5640B1E___INCLUDED_