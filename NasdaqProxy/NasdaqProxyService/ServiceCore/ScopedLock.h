// IScopedLock is an attempt to standardize the operations of different mutexes

#pragma once

namespace NasdaqProxy 
{
	class IScopedLock
	{
	public:
		virtual void Acquire() = 0;
		virtual void Release() = 0;
		virtual ~IScopedLock() {}
	};
}