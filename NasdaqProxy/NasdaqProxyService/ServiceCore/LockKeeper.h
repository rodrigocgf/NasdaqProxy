#pragma once

namespace NasdaqProxy
{

	template<class T>
	class LockKeeper
	{
	public:
		LockKeeper(T& t) : m_t(t) { m_t.Acquire(); }
		~LockKeeper() { m_t.Release(); }

	private:
		T& m_t;
	};

	template<class T>
	class LockKeeperEx
	{
	public:
		LockKeeperEx(T& t, void *p) : m_t(t), m_p(p) { m_t.Acquire(m_p); }
		~LockKeeperEx() { m_t.Release(m_p); }

	private:
		T& m_t;
		void *m_p;
	};

}