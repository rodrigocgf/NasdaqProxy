
#ifndef SINGLETON_H__41B78AC8_483D_406E_A200_0F32F589A021__INCLUDED_
#define SINGLETON_H__41B78AC8_483D_406E_A200_0F32F589A021__INCLUDED_

#include <stdlib.h>

#include "SingletonStdCreationPolicy.h"
#include "NonMTLocker.h"
#include "LockKeeper.h"



namespace NasdaqProxy 
{

	template<	typename T, typename CreationPolicy = CSingletonStdCreationPolicy<T>, typename MTLockerType = CNonMTLocker>
	class CSingleton : public CreationPolicy
	{
	public : 
		static T& getInstance();
	protected : 
		inline explicit CSingleton() {}
		inline virtual ~CSingleton() {}
	private : 
		static void ScheduleForDestruction(void (*)());
		static void Destroy();
		inline explicit CSingleton(CSingleton const&) {}
		inline CSingleton& operator=(CSingleton const&) { return *this; }
	private:
		static T* instance_;
		static MTLockerType m_objMTLocker;
		typedef LockKeeper<MTLockerType> MTLocker;
	};    

	///<summary>
	/// Implementation of CSingleton
	///</summary>

	template<typename T, typename C, typename M>
	typename T& CSingleton<T,C,M>::getInstance() 
	{
		if ( CSingleton::instance_ == 0 )
		{
			MTLocker Lock(m_objMTLocker);
			if ( CSingleton::instance_ == 0 ) {
				// CreateInstance: must be implemented in the derived Creation Policy class
				CSingleton::instance_ = CreateInstance();
				ScheduleForDestruction(CSingleton::Destroy);
			}
		}
		return *(CSingleton::instance_);
	}

	template<typename T, typename C, typename M>
	void CSingleton<T,C,M>::Destroy()
	{
		MTLocker Lock(m_objMTLocker);
		
		if ( CSingleton::instance_ != 0 ) {
			// DestroyInstance: must be implemented in the derived Creation Policy class
			DestroyInstance(CSingleton::instance_);
			CSingleton::instance_ = 0;
		}

	}

	template<typename T, typename C, typename M>
	inline void CSingleton<T,C,M>::ScheduleForDestruction(void (*pFun)()) 
	{
		atexit(pFun);
	}

	template<typename T, typename C, typename M>
	T* CSingleton<T,C,M>::instance_ = 0;


	template<typename T, typename C, typename M>
	M CSingleton<T,C,M>::m_objMTLocker;

} 

#endif // SINGLETONEX_H__41B78AC8_483D_406E_A200_0F32F589A021__INCLUDED_