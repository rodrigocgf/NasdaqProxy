
#ifndef BMF_WIN32_UTILS_H
#define BMF_WIN32_UTILS_H

#include <tchar.h>
#include <windows.h>
#include "tstring.h"
#include "Win32Exception.h"

namespace NasdaqProxy 
{


// Function: FindFullPathName
// Description: verifies if a file exists

bool FileExists(LPCTSTR lpcszFileName);

// Function: FindFullPathName
// Description: verifies if lpcszInputPath is an existing full path or an existing relative path
//				based on current directory
//				result path is returned in strFullPathName based on the process current directory

bool FindFullPathName(LPCTSTR lpcszInputPath, _tstring &strFullPathName);


/////////////////////////////////////////////////////////////////////////////
// Utility Function - ATL
// RETURNS APPLICATION FULLY QUALIFIED PATH

void getAppPath(TCHAR *pPath);

_tstring GetWin32ErrorText(DWORD ErrorCode);

_tstring CreateGUID();

const TCHAR EVENT_SOURCE_REG_PATH[] = _T("SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\%s");

DWORD AddEventSource(LPCTSTR szEventSourceName, LPCTSTR szModulePath, DWORD dwCategoryCount, bool bSupportParameterMessageFile);

void EnableConsole(BOOL bEnable);

bool HasValidDirWithReadAndWritePermission(LPCTSTR szFilePath);

} // namespace NasdaqProxy 

#endif // BMF_WIN32_UTILS_H
