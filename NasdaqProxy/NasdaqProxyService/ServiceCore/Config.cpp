#include "stdafx.h"

#include "Config.h"

#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/dom/DOMConfiguration.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMLocator.hpp>
#include <xercesc/dom/DOMLSParser.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMXPathEvaluator.hpp>
#include <xercesc/dom/DOMXPathException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/Base64.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XmlException.hpp>
#include <xercesc/sax/SaxParseException.hpp>

#include "StrX.hpp"
#include "Exceptions.hpp"
#include "..\NasdaqParser\Logger.h"

XERCES_CPP_NAMESPACE_USE;

namespace NasdaqProxy
{

	Config::Config(void)
	{

	}

	Config::~Config(void)
	{

	}

	void Config::LoadConfiguration()
	{		
		TCHAR szDrive[_MAX_DRIVE];
		TCHAR szDir[_MAX_DIR];
		TCHAR szFname[_MAX_FNAME];
		TCHAR szExt[_MAX_EXT];
		TCHAR szModuleFilePath[MAX_PATH];
		::GetModuleFileName(NULL, szModuleFilePath, MAX_PATH);
		_tsplitpath( szModuleFilePath, szDrive, szDir, szFname, szExt );
		
		TCHAR szAppPath[MAX_PATH];
		_tcsncpy(szAppPath, szDrive, MAX_PATH-1);
		_tcsncat(szAppPath, szDir, __min( _tcslen(szDir), MAX_PATH-_tcslen(szAppPath)-1));
		_tcscat(szAppPath,"NasdaqProxyConfig.xml");

		try
		{
			parseXML(szAppPath, true, m_document);
		}
		catch(NasdaqException& ex)
		{
			std::cout << "Error parsing NasdaqProxyConfig.xml . ";
			Logger::logerr('C',"Error parsing NasdaqProxyConfig.xml . ");
		}
		

		DOMElements serviceElements = evalXPath (m_document, L"/NasdaqProxyService");
		
		for ( DOMElements::iterator it = serviceElements.begin(); it !=  serviceElements.end() ; it++)
		{
			DOMElements innerElements = childrenElements((*it));

			for( DOMElements::iterator itInner = innerElements.begin() ; itInner != innerElements.end() ; itInner++ )
			{
				if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "NasdaqIpAddress" ) )
				{
					m_strNasdaqIpAddress = getAttribute ((*itInner), L"value");
				} 
				else if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "NasdaqTcpPort" ) )
				{
					m_strNasdaqTcpPort = getAttribute ((*itInner), L"value");
				}
				else if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "User" ) )
				{
					m_strUserName = getAttribute ((*itInner), L"value");
				}
				else if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "Password" ) )
				{
					m_strPassword = getAttribute ((*itInner), L"value");
				}
				else if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "ReceptionMode" ) )
				{
					m_ReceptionMode = getAttribute ((*itInner), L"value");
				}
				else if ( verifyTypeOf( (DOMElementPtr)(*itInner) , "Log") )
				{
					DOMElements logElements = childrenElements((*itInner));

					for( DOMElements::iterator itLog = logElements.begin() ; itLog != logElements.end() ; itLog++ )
					{
						if ( verifyTypeOf( (DOMElementPtr)(*itLog) , "ControlLogPath" ) )
						{
							m_strControlLogPath = getAttribute ((*itLog), L"value");
						}
						else if ( verifyTypeOf( (DOMElementPtr)(*itLog) , "ControlLogName") )
						{
							m_strControlLogName = getAttribute ((*itLog), L"value");
						}
						else if ( verifyTypeOf( (DOMElementPtr)(*itLog) , "DataLogPath") )
						{
							m_strDataLogPath = getAttribute ((*itLog), L"value");
						}
						else if ( verifyTypeOf( (DOMElementPtr)(*itLog) , "DataLogName") )
						{
							m_strDataLogName = getAttribute ((*itLog), L"value");
						}
						else if ( verifyTypeOf( (DOMElementPtr)(*itLog) , "LogLevel") )
						{
							if ( !getAttribute ((*itLog), L"enableData").compare("true") )
								m_bLogData = true;
							else
								m_bLogData = false;

							if ( !getAttribute ((*itLog), L"enableControl").compare("true") )
								m_bLogControl = true;
							else
								m_bLogControl = false;

							if ( !getAttribute ((*itLog), L"enableErrors").compare("true") )
								m_bLogErrors = true;
							else
								m_bLogErrors = false;

							if ( !getAttribute ((*itLog), L"enableTrace").compare("true") )
								m_bLogTrace = true;
							else
								m_bLogTrace = false;
						}
						
					}
				}
			}
			
		}
	}


	bool Config::verifyTypeOf(DOMElementPtr parentElement , const string& elementName)
	{
		if ( StrX::equals (parentElement->getNodeName(), elementName) )
			return true;
		else
			return false;		
	}

	bool Config::hasElement(DOMElementPtr parentElement ,const wstring& elementName)
	{
		DOMNodeList* pAttr = parentElement->getElementsByTagName(elementName.c_str());
		if ( pAttr == NULL )
			return false;

		if ( pAttr->getLength() > 0 )
			return true;
		else
			return false;
	}

	string Config::getAttribute (const DOMElement * element, const wstring& attributeName)
	{
		return StrX::toString (element->getAttribute (attributeName.c_str()));
	}

	string Config::getAttribute (const DOMElement * element, const wstring& attributeName, const string& defaultValue)
	{
		DOMAttr* pAttr = element->getAttributeNode (attributeName.c_str());
		return pAttr ? StrX::toString (pAttr->getValue()) : defaultValue;
	}

	const DOMElement * Config::childElement(const DOMElement * root )
	{
		DOMNode * pChild = root->getFirstChild();
		if (pChild->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			DOMElement *childElement = dynamic_cast<DOMElement*> (pChild);
			return childElement;
		}

		return NULL;
	}

	vector<const DOMElement*> Config::childrenElements (const DOMElement * root)
	{
		vector<const DOMElement*> ret;
		DOMNodeList* children = root->getChildNodes ();
		for (XMLSize_t i = 0, n = children->getLength(); i < n; ++i)
		{
			DOMNode* child = children->item(i);
			if (child->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				DOMElement *childElement = dynamic_cast<DOMElement*> (child);
				if (childElement)
				{
					ret.push_back(childElement);
				}
			}
		}

		return ret;
	}

	vector<const DOMElement*> Config::childrenElements (const DOMElement * root, const string& nodeName)
	{
		vector<const DOMElement*> ret;
		DOMNodeList* children = root->getChildNodes ();
		for (XMLSize_t i = 0, n = children->getLength(); i < n; ++i)
		{
			DOMNode* child = children->item(i);
			if (child->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				DOMElement *childElement = dynamic_cast<DOMElement*> (child);
				if (childElement && StrX::equals (childElement->getNodeName(), nodeName))
				{
					ret.push_back(childElement);
				}
			}
		}

		return ret;
	}

	vector<const DOMElement*> Config::evalXPath (const XERCES_CPP_NAMESPACE::DOMDocument* document, const wstring& xpathExpression)
	{
		DOMXPathEvaluator *xpe = const_cast<DOMXPathEvaluator *>(dynamic_cast<const DOMXPathEvaluator *>(document));
	
		vector<const DOMElement*> ret;
		try {			

			DOMXPathResult* result = xpe->evaluate (xpathExpression.c_str(), document->getDocumentElement(),
				NULL, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, NULL);
			for (int i = 0; result->snapshotItem (i); ++i)
			{
				DOMElement* element = dynamic_cast<DOMElement*> (result->getNodeValue ());
				if (element) {
					ret.push_back (element);
				}
			}

			result->release ();
		} catch (DOMXPathException& ) {
			;
		} catch (DOMException& ) {
			;
		}
		return ret;
	}

	bool Config::xerces_initialized = false;	

	void Config::parseXML(const string& filename, bool validate, XERCES_CPP_NAMESPACE::DOMDocument*& document)
	{
		if (!xerces_initialized)
		{
			try {
				XMLPlatformUtils::Initialize();
			} catch (const XMLException& ex) 
			{				
				throw XercesInitializationError () 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< filelocation_info (str_file_location (ex.getSrcFile(), (int) ex.getSrcLine()))
				<< PROGRAMLOCATION();
				
			}
			xerces_initialized = true;
		}    
		XercesDOMParser *parser = new XercesDOMParser();
		if (validate)
		{
			parser->setDoSchema (true);
			parser->setExitOnFirstFatalError(true);
			parser->setDoNamespaces (true);			
			parser->setValidationSchemaFullChecking (true);
		}

		// Create error handler
		CXMLDebugErrorHandler errorHandler;
		parser->setErrorHandler (&errorHandler);
		errorHandler.resetErrors();

		// Parse the file
		document = NULL;
		try 
		{
			parser->parse (filename.c_str());
			document = parser->getDocument();
		}		
		catch (const XMLException& ex)
		{
			throw InvalidTemplateError() 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< filelocation_info (str_file_location (ex.getSrcFile(), (int) ex.getSrcLine()))
				<< PROGRAMLOCATION();
			
		}
		catch (const DOMException& ex)
		{					

			throw InvalidTemplateError() 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< code_info (ex.code)
				<< PROGRAMLOCATION();
			
		}	
		catch ( const SAXParseException& ex)
		{	
			throw InvalidTemplateError() 
				<< descr_info (StrX::toString(ex.getMessage())) 				
				<< PROGRAMLOCATION();
		}
		catch (...)
		{	
			throw InvalidTemplateError() 
				<< descr_info ("Unexpected error!") 
				<< PROGRAMLOCATION();
			
		}
		parser->reset();
	}

}