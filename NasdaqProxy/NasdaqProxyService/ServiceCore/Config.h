#pragma once

#include <string>
#include <vector>
#include <math.h>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <boost/exception.hpp>
#include <xercesc/sax/ErrorHandler.hpp>



namespace NasdaqProxy
{
	using namespace std;
	XERCES_CPP_NAMESPACE_USE;	

	class Config
	{
	public:
		Config(void);
		~Config(void);
		Config(Config & config) 
		{
			m_strUserName			= config.m_strUserName;
			m_strPassword			= config.m_strPassword;
			m_strNasdaqIpAddress	= config.m_strNasdaqIpAddress;
			m_strNasdaqTcpPort		= config.m_strNasdaqTcpPort;
			m_ReceptionMode			= config.m_ReceptionMode;

			m_strControlLogPath		= config.m_strControlLogPath;
			m_strControlLogName		= config.m_strControlLogName;
			m_strDataLogPath		= config.m_strDataLogPath;
			m_strDataLogName		= config.m_strDataLogName;

			m_bLogData				= config.m_bLogData;
			m_bLogControl			= config.m_bLogControl;
			m_bLogErrors			= config.m_bLogErrors;
			m_bLogTrace				= config.m_bLogTrace;

		}

		void LoadConfiguration();

		string	m_strUserName;
		string	m_strPassword;
		string	m_strNasdaqIpAddress;
		string	m_strNasdaqTcpPort;
		string	m_ReceptionMode;
		
		string	m_strControlLogPath;
		string	m_strControlLogName;
		string	m_strDataLogPath;
		string	m_strDataLogName;

		bool	m_bLogData;
		bool	m_bLogControl;
		bool	m_bLogErrors;
		bool	m_bLogTrace;

		XERCES_CPP_NAMESPACE::DOMDocument * m_document;

		typedef const XERCES_CPP_NAMESPACE::DOMElement* DOMElementPtr;
		typedef vector<DOMElementPtr> DOMElements;

		static bool verifyTypeOf(DOMElementPtr parentElement , const string& elementName);
		static bool hasElement(DOMElementPtr parentElement ,const wstring& elementName);
		static string getAttribute (DOMElementPtr element, const wstring& attributeName);
		static string getAttribute (DOMElementPtr element, const wstring& attributeName, const string& defaultValue);
		static DOMElementPtr childElement(const DOMElement * root );
		static DOMElements childrenElements (DOMElementPtr root);
		static DOMElements childrenElements (DOMElementPtr root, const string& nodeName);
		static DOMElements evalXPath (const XERCES_CPP_NAMESPACE::DOMDocument* document, const wstring& xpathExpression);
		static void parseXML(const string& filename, bool validate, XERCES_CPP_NAMESPACE::DOMDocument*& document);    

		static bool xerces_initialized;		
	};


}