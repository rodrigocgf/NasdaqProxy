#pragma once

#include "tstring.h"
#include "Exception.h"
#include "EventMessages.h"

#ifndef _WIN32_DCOM
#define _WIN32_DCOM 
#endif

#include <objbase.h>

#pragma warning( disable : 4290 )

// gathered information at 
// http://codeproject.com/system/mctutorial.asp
// http://www.codeguru.com/Cpp/COM-Tech/atl/atl/article.php/c3553
// http://www.codeproject.com/system/eventlogging.asp

namespace NasdaqProxy 
{
	class CEventLogging
	{
	public:
		static void AddEventSource(LPCTSTR szSourceName, DWORD dwCategoryCount = 0, bool bSupportParameterMessageFile = false, DWORD dwCoInit = COINIT_APARTMENTTHREADED) throw( CException );
		static void RemoveEventSource(LPCTSTR szSourceName) throw( CException );
		static bool DoesEventSourceExist(LPCTSTR szSourceName);
		////////////////////////////////////////////////
		// Function: LogEvent
		// Author:    Djalma
		// Reference: http://www.codeguru.com/Cpp/COM-Tech/atl/atl/article.php/c3553
		// I have modified the LogEvent() function to take different EventTypes, EventID, and string that are to be written.
		// Created:   22/11/2005
		// Updated:   01/06/2006
		// Parameters:
		//		szSourceName
		//		[in] Event Source Name
		//		wType 
		//		[in] Type of event to be logged. This parameter can be one of the following values:
		//				WORD wType options:
		//				EVENTLOG_SUCCESS
		//				EVENTLOG_ERROR_TYPE
		//				EVENTLOG_WARNING_TYPE 
		//				EVENTLOG_INFORMATION_TYPE
		//				EVENTLOG_AUDIT_SUCCESS
		//				EVENTLOG_AUDIT_FAILURE
		//		wCategory
		//		[in] Event category. This is source-specific information; the category can have any value
		//		dwEventID
		//		[in] Event identifier. The event identifier specifies the entry in the message file associated with the event source. 
		////////////////////////////////////////////
		static void LogEvent(LPCTSTR szSourceName, WORD wType, WORD wCategory, DWORD dwEventID, LPCTSTR pFormat,...);
	};
} // namespace NasdaqProxy 