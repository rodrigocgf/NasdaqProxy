#pragma once

#include "Exception.h"

///	<summary>
///		Class derived from CException to handling Win32 errors codes
///	</summary>
///	<creation>
///		11/02/2005
///	</creation>
///	<author>
///		Rafael Lucyk
///	</author>

namespace NasdaqProxy
{

	class CWin32Exception : public CException
	{
	public:
		CWin32Exception(const _tstring &szWhere, DWORD errId, LPCTSTR szFile = NULL, int nLine = 0, LPCTSTR szFunction = NULL) 
			: CException(szWhere, errId, szFile, nLine, szFunction){}

		virtual ~CWin32Exception(void) {};

		virtual _tstring getMessage() const;
	};
}

#define VERIFY_THROW_WIN32(condition, szWhere, errId)\
	if (condition)\
		throw NasdaqProxy::CWin32Exception(szWhere, errId, _T(__FILE__), __LINE__, _T(__FUNCTION__));

#define THROW_WIN32_EXCEPTION(szWhere, errId)\
	throw NasdaqProxy::CWin32Exception(szWhere, errId, _T(__FILE__), __LINE__, _T(__FUNCTION__));