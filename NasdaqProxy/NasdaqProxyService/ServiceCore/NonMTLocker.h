#ifndef NONMTLOCKER_H__FBB2B091_2BBD_42C6_AC8F_17CDB150E7CD__INCLUDED_
#define NONMTLOCKER_H__FBB2B091_2BBD_42C6_AC8F_17CDB150E7CD__INCLUDED_

#include "ScopedLock.h"

namespace NasdaqProxy 
{
	class CNonMTLocker : public IScopedLock
	{
	public:
		CNonMTLocker() {}
		~CNonMTLocker() {}
		inline void Acquire() {}
		inline void Release() {}
	};

}

#endif // NONMTLOCKER_H__FBB2B091_2BBD_42C6_AC8F_17CDB150E7CD__INCLUDED_
