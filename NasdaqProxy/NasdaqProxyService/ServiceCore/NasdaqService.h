#ifndef _NASDAQ_SERVICE_H_
#define _NASDAQ_SERVICE_H_

#pragma once

#include "WinService.h"
#include <set>
#include "..\NasdaqParser\ReceiverFactory.h"
#include "Config.h"
#include "..\TibcoCAML.h"

namespace NasdaqProxy 
{

	using namespace std;

	class CNasdaqService : public CWinService
	{
	public:
		CNasdaqService(void);
		~CNasdaqService(void);

		BOOL Startup();

		shared_ptr<INASDAQReceiver> receiverPtr;
		void SendToTibco(unsigned char * buffer, int size );

		bool OnStart();
		void OnStop();

		void Run();

		Config		m_Config;
		CTibcoCAML	m_TibcoCaml;
	private:
		
		void OnPause();
		void OnContinue();
		BOOL OnCustomStartupCommand(LPCTSTR cszCmd, LPCTSTR cszExtraInfo);
		static unsigned int __stdcall Execute(void *pvParam);
		
	private:
		static const TCHAR * szServiceName;
		static const TCHAR * szServiceDescription;
		static const TCHAR * szServiceDisplayName;
	private:
		HANDLE m_hEvStopRouter;
		HANDLE m_hThService;
	};


}

#endif