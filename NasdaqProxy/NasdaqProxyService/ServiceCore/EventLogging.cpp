#include "stdafx.h"

#include "dbg.h"
#include "Eventlogging.h"
#include "BmfWin32Utils.h"
#include "Win32Exception.h"

#ifndef _WIN32_DCOM
#define _WIN32_DCOM 
#endif

#include <objbase.h>

#import "BTSWinEventLog/BTSWinEventLog.tlb" no_namespace, named_guids, raw_interfaces_only

namespace NasdaqProxy
{

	void CEventLogging::AddEventSource(LPCTSTR szSourceName, DWORD dwCategoryCount, bool bSupportParameterMessageFile, DWORD dwConit)
	throw( CException )
	{
		// workaround with coinitializeEx that do not compile in x64
		#if defined(_WIN64)
			HRESULT hr = CoInitialize(NULL);
		#else
			//HRESULT hr = CoInitializeEx(NULL, dwConit);
			HRESULT hr = CoInitialize(NULL);
		#endif
		ASSERT(SUCCEEDED(hr));
		VERIFY_THROW_WIN32(FAILED(hr), _T("Error adding event source"), hr);
		{
			IEventSourcePtr pEventSrc = NULL;
			HRESULT hr = pEventSrc.CreateInstance(__uuidof(EventSource));
			VERIFY_THROW_WIN32(FAILED(hr), _T("Error adding event source"), hr);

			hr = pEventSrc->CreateEventSource(
										_bstr_t(szSourceName), 
										dwCategoryCount, 
										bSupportParameterMessageFile==true?VARIANT_TRUE:VARIANT_FALSE);
		}
		::CoUninitialize();
		VERIFY_THROW_WIN32(FAILED(hr), _T("Error adding event source"), hr);
	}

	void CEventLogging::RemoveEventSource(LPCTSTR szSourceName)
	throw( CException )
	{
		DWORD dwError = 0;
		TCHAR szRegPath[ MAX_PATH ];

		ASSERT(szSourceName !=  NULL);
		ASSERT(_tcscmp(szSourceName,_T("")) != 0);
		
		_stprintf( szRegPath, EVENT_SOURCE_REG_PATH, szSourceName );
		dwError = RegDeleteKey( HKEY_LOCAL_MACHINE, szRegPath );
		VERIFY_THROW_WIN32((ERROR_SUCCESS != dwError), _T("Error removing Event Source"), dwError);
	}

	bool CEventLogging::DoesEventSourceExist(LPCTSTR szSourceName)
	{
		HKEY hRegKey = NULL;
		DWORD dwError = 0;
		TCHAR szRegPath[ MAX_PATH ];

		ASSERT( szSourceName !=  NULL );
		//ASSERT( strcmp(szSourceName, _T("")) != 0 );
		ASSERT( _tcscmp(szSourceName, _T("")) != 0 );

		_stprintf( szRegPath, EVENT_SOURCE_REG_PATH, szSourceName );
		dwError = RegOpenKeyEx( HKEY_LOCAL_MACHINE, szRegPath, 0, KEY_READ, &hRegKey );
		return (ERROR_SUCCESS == dwError);
	}

	void CEventLogging::LogEvent(LPCTSTR szSourceName, WORD wType, WORD wCategory, DWORD dwEventID, LPCTSTR pFormat,...)
	{
		TCHAR    szMsg[4096];
		HANDLE   hEventSource = NULL;
		LPTSTR   lpszStrings[1];
		va_list pArg;

		va_start(pArg, pFormat);
		_vstprintf(szMsg, pFormat, pArg);
		va_end(pArg);

		lpszStrings[0] = szMsg;

		ASSERT(szSourceName !=  NULL);
		//ASSERT(strcmp(szSourceName,_T("")) != 0);
		ASSERT( _tcscmp(szSourceName,_T("")) != 0);

		// Get a handle to use with ReportEvent().
		hEventSource = RegisterEventSource(NULL, szSourceName); 
		ASSERT( NULL != hEventSource);
		if ( NULL != hEventSource )
		{
			// Write to event log.
			ReportEvent(hEventSource, wType, wCategory, dwEventID, NULL, 1, 0, (LPCTSTR*) &lpszStrings[0], NULL);
			DeregisterEventSource(hEventSource);
		}
	}

} // namespace NasdaqProxy