#pragma once

#define _WINSOCKAPI_ 
// _WINSOCKAPI_  prevents inclusion of winsock in windows.h
#include <windows.h>
#include <shellapi.h>
#include <sstream>
#include "EventMessages.h"
#include "Exception.h"
#include "tstring.h"
#include "dbg.h"
#include "EventLogging.h"

#define SVR_MSG "Usage: %s -d -i -r -s -? -tag tagname -conf xmlfile\n"\
	"-d -\tDebug gateway\n"\
	"-s -\tSilent; display no message boxes\n"\
	"-i -\tInstall service\n"\
	"-r -\tUninstall service\n"\
	"-? -\tShow this help\n"

#include <crtdbg.h>


namespace NasdaqProxy
{

	using namespace std;

	class CWinService
	{
	public:		
		virtual			~CWinService();			
		virtual BOOL	Startup(); // entry point for service activities
		
		bool			IsRunning()  const  { return getServiceInstance()->m_dwCurrentState == SERVICE_RUNNING; }
		bool			IsPaused()   const  { return getServiceInstance()->m_dwCurrentState == SERVICE_PAUSED; }
		DWORD			GetStatus() const  { return m_dwCurrentState; }
		bool			IsDebugMode() const { return m_bDebugMode; }
		bool			IsSilentMode() { return m_bSilentMode; }
		
		bool			IsInstalled() const;
		void			HaltService(); // use this function whenever one thread might want to stop the whole service
		static void		LogEvent(WORD wType, WORD wCategory, DWORD dwID,LPCTSTR pFormat,...);
	protected:
		struct tagNTEventSourceInfo
		{
			tagNTEventSourceInfo(	const TCHAR* szDefaultEvSrcName = NULL,
									bool bUseExternalPE4MessageResource = true,
									DWORD dwCategoryCount = 0,
									bool bSupportParameterMessageFile = false):
				m_dwCategoryCount(dwCategoryCount),
				m_bSupportParameterMsgFile(bSupportParameterMessageFile),
				m_strDefaultEventSourceName(szDefaultEvSrcName!=NULL?szDefaultEvSrcName:_T("")),
				m_bUseExternalPE4MessageResource(bUseExternalPE4MessageResource)
			{}

			const DWORD		m_dwCategoryCount;
			const bool		m_bSupportParameterMsgFile;
			const bool		m_bUseExternalPE4MessageResource;
			const _tstring	m_strDefaultEventSourceName;
		};

		CWinService (	const TCHAR* cszServiceName, const TCHAR* szServiceDescription, const TCHAR* cszDisplayName,
						tagNTEventSourceInfo *pSourseInfo = NULL,
						DWORD dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE | SERVICE_ACCEPT_SHUTDOWN,
						DWORD dwCoInit = COINIT_APARTMENTTHREADED
					);

		// Abstract methods, must be implemented on derived class
		virtual bool		OnStart() = 0;
		virtual void		OnPause() = 0;
		virtual void		OnContinue() = 0;
		virtual void		OnStop() = 0;
		virtual BOOL		OnCustomStartupCommand(LPCTSTR cszCmd, LPCTSTR cszExtraInfo) {return FALSE;}
		bool				SetStatus(	DWORD dwState, DWORD dwWin32ExitCode = NO_ERROR,
										DWORD dwServiceSpecificExitCode = 0, DWORD dwCheckPoint = 0, DWORD dwWaitHint = 0);

		void				SetServiceName(const TCHAR *szServiceName, const TCHAR *szServiceDescription, const TCHAR *szShortName);
		void				AddStartParam(const TCHAR* szParam);
		inline void			KillService() { if (NULL != m_hEvKillService) ::SetEvent(m_hEvKillService); }
	private:
		inline static CWinService* getServiceInstance()
		{ 
			ASSERT(m_pServiceInstance != NULL);
			return m_pServiceInstance;
		}

		void ShowMessage(LPCTSTR szText, LPCTSTR szCaption, UINT nType)	
		{ if (!m_bSilentMode) ::MessageBox(NULL, szText, szCaption, nType);}
		
		BOOL ParseStartupCommands();
	private:
		// methods related to service management
		static void WINAPI		ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv);
		static void WINAPI		ServiceCtrlHandler(DWORD dwOpcode);		
		// Deprecated:
		static UINT __stdcall	WaitForKeyboard (LPVOID lpParam);			

		void					Run();
		bool					StopService();
		
		// methods related to installation of the Service
		BOOL					InstallService();
		BOOL					DeleteService();
		void					AddEventSource(const TCHAR* szEventSourceName, DWORD dwCategoryCount, bool bSupportParameterMessageFile);
		void					RemoveEventSource();
		
		// methods to start the service in different mode
		BOOL					StartDispatcher();
		BOOL					DebugService();
		BOOL					Usage();		
	private:
		// private data members
		bool					m_bDebugMode;
		bool					m_bSilentMode;
		tagNTEventSourceInfo	m_NTEventSourseInfo;

		SERVICE_STATUS_HANDLE	m_serviceStatusHandle;

		DWORD					m_dwCurrentState;
		DWORD					m_dwControlsAccepted;
		
		TCHAR					m_szServiceName[100];
		TCHAR					m_szDisplayName[100];
		TCHAR					m_szServiceDescription[200];
		TCHAR					m_szStartParams[2*MAX_PATH];

		HANDLE					m_hEvKillService;
		HANDLE					m_hThDebug;
		DWORD					m_dwConit;
		static CWinService		*m_pServiceInstance;
		static _tstring			m_strDefaultEventSourceName;
	};

}