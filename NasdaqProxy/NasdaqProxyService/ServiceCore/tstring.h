#pragma once

///	<summary>
///		Typedef to use std:string like a TCHAR. 
///	</summary>
///	<creation>
///		11/05/2005
///	</creation>
///	<author>
///		Rafael Lucyk
///	</author>

#include <string>

//using namespace std;

#ifdef _UNICODE
	typedef std::wstring _tstring;
#else
	typedef std::string _tstring;
#endif
