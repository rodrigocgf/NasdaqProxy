#include "stdafx.h"

#include "dbg.h"
#include "Win32Exception.h"

namespace NasdaqProxy
{
	_tstring CWin32Exception::getMessage() const
	{
		_tstring result;

		LPTSTR* pBuffer = NULL;
		int ret;

		DWORD flags = FORMAT_MESSAGE_IGNORE_INSERTS
					| FORMAT_MESSAGE_FROM_SYSTEM
					| FORMAT_MESSAGE_ALLOCATE_BUFFER;

		ASSERT(m_errId != NOERROR);

		ret = FormatMessage(flags, 0, m_errId, 0, reinterpret_cast<LPTSTR>(&pBuffer), 0, 0);

		ASSERT(pBuffer != NULL);

		if ((0 == ret) || 
			(NULL == pBuffer)){
			result = _T("[Unable to fetch error text]");
		}
		else{
			result = reinterpret_cast<TCHAR *>(pBuffer);
			LocalFree(pBuffer);
		}

		return result;
	}

}