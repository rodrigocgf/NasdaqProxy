#include "stdafx.h"

#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <comdef.h>
#include "dbg.h"
#include "tstring.h"
#include "BmfWin32Utils.h"
#include "Win32Exception.h"

namespace NasdaqProxy
{

	///<summary>
	/// Function: FindFullPathName
	/// Description: verifies if a file exists
	///</summary>
	bool FileExists(LPCTSTR lpcszFileName)
	{
		if(_taccess(lpcszFileName,0) == -1)
			return false;
		return true;
	}

	///<summary>
	/// Function: FindFullPathName
	/// Description: verifies if lpcszInputPath is an existing full path or an existing relative path
	///				based on current directory
	///				result path is returned in strFullPathName based on the process current directory
	///</summary>
	bool FindFullPathName(LPCTSTR lpcszInputPath, _tstring &strFullPathName)
	{	
		if (FileExists(lpcszInputPath)) 
		{
			strFullPathName = _tstring(lpcszInputPath);
			return true;
		}
		
		TCHAR szPath[MAX_PATH];
		DWORD dwRet = ::GetCurrentDirectory(MAX_PATH, szPath);
		ASSERT(dwRet!=0);
		
		strFullPathName = _tstring(szPath) + _tstring(_T("\\")) + _tstring(lpcszInputPath);
		
		dwRet = ::GetFullPathName(strFullPathName.c_str(), sizeof(szPath) / sizeof(TCHAR), szPath, NULL);
		ASSERT(dwRet!=0);
		strFullPathName = szPath;
		return FileExists(strFullPathName.c_str());
	}


	///<summary>
	/// Utility Function
	/// returns application fully qualified path
	///</summary>
	void getAppPath(TCHAR *pPath)
	{
		TCHAR szFilePath[_MAX_PATH];	
		TCHAR drive[_MAX_DRIVE];
		TCHAR dir[_MAX_DIR];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];

		::GetModuleFileName(NULL, szFilePath, _MAX_PATH);		
		_tsplitpath(szFilePath, drive, dir, fname, ext );		
		_tcscpy(pPath,drive);
		_tcscat(pPath,dir);
	}

	_tstring GetWin32ErrorText(DWORD ErrorCode)
	{
  		LPVOID lpMsgBuf;
		FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			ErrorCode,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
			);
		
		_tstring strRet = (LPCTSTR) lpMsgBuf;
		LocalFree( lpMsgBuf );
		return strRet;
	}


	_tstring CreateGUID()
	{
		GUID guid = GUID_NULL;
		HRESULT hr = ::CoCreateGuid(&guid);	
		const TCHAR szGUIDFormat[] = _T("{%08lX-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}");
		TCHAR szGUIDBuff[39];
		_sntprintf(szGUIDBuff, 39, szGUIDFormat,
					guid.Data1,
					guid.Data2,
					guid.Data3,
					guid.Data4[0],
					guid.Data4[1],
					guid.Data4[2],
					guid.Data4[3],
					guid.Data4[4],
					guid.Data4[5],
					guid.Data4[6],
					guid.Data4[7]);
		return szGUIDBuff;
	}


	DWORD AddEventSource(LPCTSTR szEventSourceName, LPCTSTR szModulePath, DWORD dwCategoryCount, bool bSupportParameterMessageFile)
	{
		HKEY hRegKey = NULL;
		DWORD dwError = 0;
		TCHAR szRegPath[ MAX_PATH ];

		ASSERT(szEventSourceName !=  NULL);
		ASSERT(_tcscmp(szEventSourceName,_T("")) != 0);
		ASSERT(szModulePath !=  NULL);
		ASSERT(_tcscmp(szModulePath,_T("")) != 0);
		
		_stprintf( szRegPath, EVENT_SOURCE_REG_PATH, szEventSourceName );

		// Create the event source registry key
		dwError = RegCreateKey( HKEY_LOCAL_MACHINE, szRegPath, &hRegKey );
		if (ERROR_SUCCESS != dwError)
			return dwError;

		// Register EventMessageFile
		dwError = RegSetValueEx( hRegKey,
				  _T("EventMessageFile"), 0, REG_EXPAND_SZ,
				  (PBYTE) szModulePath, (DWORD)(_tcslen( szModulePath) + 1) * sizeof TCHAR );

		if (ERROR_SUCCESS != dwError)
			return dwError;

		// Register supported event types
		DWORD dwTypes = EVENTLOG_ERROR_TYPE |
			  EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;
		dwError = RegSetValueEx( hRegKey, _T("TypesSupported"), 0, REG_DWORD,
								(LPBYTE) &dwTypes, sizeof dwTypes );
		if (ERROR_SUCCESS != dwError)
			return dwError;

		// If we want to support event categories,
		// we have also to register the CategoryMessageFile.
		// and set CategoryCount. Note that categories
		// need to have the message ids 1 to CategoryCount!

		if( dwCategoryCount > 0 ) {

			dwError = RegSetValueEx( hRegKey, _T("CategoryMessageFile"),
					  0, REG_EXPAND_SZ, (PBYTE) szModulePath,
					  (DWORD)(_tcslen( szModulePath) + 1) * sizeof TCHAR );
			if (ERROR_SUCCESS != dwError)
				return dwError;

			dwError = RegSetValueEx( hRegKey, _T("CategoryCount"), 0, REG_DWORD,
					  (PBYTE) &dwCategoryCount, sizeof dwCategoryCount );
			if (ERROR_SUCCESS != dwError)
				return dwError;

		}

		// Register Parameter Message File, if desired
		if (bSupportParameterMessageFile) {
			dwError = RegSetValueEx( hRegKey,
					  _T("ParameterMessageFile"), 0, REG_EXPAND_SZ,
				  (PBYTE) szModulePath, (DWORD)(_tcslen( szModulePath) + 1) * sizeof TCHAR );

			if (ERROR_SUCCESS != dwError) 
				return dwError;
		}

		RegCloseKey( hRegKey );
		return ERROR_SUCCESS;
	}

	void EnableConsole(BOOL bEnable)
	{
		HWND hwnd = NULL;
		/*
		TCHAR buf[100];
		wsprintf ( buf, CreateGUID().c_str() );
		SetConsoleTitle ( (LPCTSTR) buf );
		// Give this a chance - it may fail the first time through.
		GetConsoleWindow
		
		while ( NULL == hwnd ) {
			hwnd = ::FindWindowEx ( NULL, NULL, NULL, (LPCTSTR) buf );
		}
		ShowWindow ( hwnd, bEnable?SW_SHOW:SW_HIDE );
		*/
		
		typedef HWND (WINAPI *tGetConsoleWindow)(void);
		tGetConsoleWindow pGetConsoleWindow = 0;
		HINSTANCE handle =  ::LoadLibrary(_T("Kernel32.dll"));
		if ( handle ) {
			pGetConsoleWindow = (tGetConsoleWindow)::GetProcAddress(handle, "GetConsoleWindow");
		}
		if ( pGetConsoleWindow ) 
		{
			HWND hwnd = pGetConsoleWindow();
			ShowWindow ( hwnd, bEnable?SW_SHOW:SW_HIDE );
			UpdateWindow( hwnd );
		}
		if ( handle )
			::FreeLibrary(handle);
		
		// TODO: TRANSFORMAR O EXE EM UMA APLICA��O WIN32 E CRIAR O CONSOLE QUANDO EM DEBUG
		/*
		if (!bEnable)
			FreeConsole();
		else
			AllocConsole();
		*/
	}

	bool HasValidDirWithReadAndWritePermission(LPCTSTR szFilePath)
	{
		TCHAR szDrive[_MAX_DRIVE];
		TCHAR szDir[_MAX_DIR];
		TCHAR szFname[_MAX_FNAME];
		TCHAR szExt[_MAX_EXT];
		TCHAR szDirWithDrive[MAX_PATH];
		
		_tsplitpath( szFilePath, szDrive, szDir, szFname, szExt );
		_tcscpy(szDirWithDrive, szDrive);
		_tcscat(szDirWithDrive, szDir);
		
		if (_taccess(szDirWithDrive, 6) == -1)
			return false;
		return true;
	}

} // namespace NasdaqProxy
