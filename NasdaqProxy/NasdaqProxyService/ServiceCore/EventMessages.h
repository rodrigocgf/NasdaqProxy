////////////////////////////////////////
// Eventlog categories
//
// These always have to be the first entries in a message file
//
//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: CATEG_SERVICE
//
// MessageText:
//
//  Service Management
//
#define CATEG_SERVICE                    0x00000064L

//
// MessageId: CATEG_APP
//
// MessageText:
//
//  Application
//
#define CATEG_APP                        0x00000065L

////////////////////////////////////////
// Events
//
//
// MessageId: EVMSG_INSTALLED
//
// MessageText:
//
//  The %1 service was installed.
//
#define EVMSG_INSTALLED                  0x00000066L

//
// MessageId: EVMSG_REMOVED
//
// MessageText:
//
//  The %1 service was removed.
//
#define EVMSG_REMOVED                    0x00000067L

//
// MessageId: EVMSG_NOTREMOVED
//
// MessageText:
//
//  The %1 service could not be removed.
//
#define EVMSG_NOTREMOVED                 0x00000068L

//
// MessageId: EVMSG_CTRLHANDLERNOTINSTALLED
//
// MessageText:
//
//  The control handler could not be installed, %s
//
#define EVMSG_CTRLHANDLERNOTINSTALLED    0x00000069L

//
// MessageId: EVMSG_FAILEDINIT
//
// MessageText:
//
//  The initialization process failed
//
#define EVMSG_FAILEDINIT                 0x0000006AL

//
// MessageId: EVMSG_STARTED
//
// MessageText:
//
//  The service was started
//
#define EVMSG_STARTED                    0x0000006BL

//
// MessageId: EVMSG_STOPPED
//
// MessageText:
//
//  The service was stopped
//
#define EVMSG_STOPPED                    0x0000006CL

//
// MessageId: EVMSG_BADREQUEST
//
// MessageText:
//
//  The service received an unsupported request
//
#define EVMSG_BADREQUEST                 0x0000006DL

//
// MessageId: ENVMSG_GENERIC
//
// MessageText:
//
//  %1
//
#define ENVMSG_GENERIC                   0x0000006EL

