#pragma once

#include <string>
#include <windows.h>
#include <tchar.h>

#include "tstring.h"
#include <atlconv.h>


namespace NasdaqProxy 
{
	using namespace std;

	class CException
	{
	public: 
		CException(const _tstring &szWhere, const _tstring &message, DWORD errId = 0, LPCTSTR szFile = NULL, int nLine = 0, LPCTSTR szFunction = NULL)
			: m_where(szWhere), m_message(message), m_errId(errId), m_szFile(szFile), m_nLine(nLine), m_szFunction(szFunction) {}

		CException(const _tstring &szWhere, DWORD errId, LPCTSTR szFile = NULL, int nLine = 0, LPCTSTR szFunction = NULL)
			: m_where(szWhere), m_errId(errId), m_szFile(szFile), m_nLine(nLine), m_szFunction(szFunction) {}

		virtual ~CException() {}
		virtual _tstring getWhere() const { return m_where; }
		virtual _tstring getMessage() const { return m_message; }
		virtual _tstring getFile() const {return m_szFile;} //{ USES_CONVERSION; return A2T((LPSTR)m_szFile); }
		virtual _tstring getFunction() const {return m_szFunction;} //{ USES_CONVERSION; return A2T((LPSTR)m_szFunction); }

		virtual int getLine() const { return m_nLine; }
		virtual DWORD getErrId() const{ return m_errId; }

	protected :
		const int m_nLine;
		const DWORD m_errId;
		const _tstring m_where;
		const _tstring m_message;
		const LPCTSTR m_szFile; 
		const LPCTSTR m_szFunction;
	};

}

#define VERIFY_THROW_EXCEPTION(condition, szWhere, message, errId)\
	if(condition)\
		throw NasdaqProxy::CException(szWhere, message, errId, _T(__FILE__), __LINE__, _T(__FUNCTION__));

#define THROW_EXCEPTION(szWhere, message, errId)\
	throw NasdaqProxy::CException(szWhere, message, errId, _T(__FILE__), __LINE__, _T(__FUNCTION__));