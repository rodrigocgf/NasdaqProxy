#ifndef DBG_H_INCLUDED
#define DBG_H_INCLUDED

#include <crtdbg.h>

//debug mechanisms 

#ifdef _DEBUG
    #ifndef ASSERT
		#define ASSERT(x) _ASSERTE(x)
	#endif
	#ifndef VERIFY
		#define VERIFY(x) _ASSERTE(x)
	#endif
	#ifndef TEST
		#define TEST(x)   _ASSERTE(!(x))
	#endif
    
#else //_DEBUG
    
    #define ASSERT(x)
    #define VERIFY(x)   (x)
    #define TEST(x)     (x)

#endif //_DEBUG


#endif //DBG_H_INCLUDED