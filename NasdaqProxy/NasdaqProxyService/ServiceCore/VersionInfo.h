#ifndef _VERSION_INFO_H_
#define _VERSION_INFO_H_

#pragma once

#pragma warning(disable:4786)
#include <windef.h>
#include "tstring.h"

typedef _tstring tstring;

namespace NasdaqProxy 
{
	class CVersionInfo  
	{
	public:
		struct VersionInfo 
		{
			tstring strProductName;
			tstring strProductVersion;
			tstring strFileVersion;
			tstring strCopyright;
			tstring strComments;
		};

		static VersionInfo GetVersionInfo(tstring szFullPath, WORD wLanguage);
	};

} //namespace BTS 

#endif