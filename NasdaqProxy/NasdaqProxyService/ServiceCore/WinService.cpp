#include "stdafx.h"

#include "WinService.h"

#include <process.h>
#include <conio.h>
#include <stdio.h>
#include <tchar.h>
#include <atlbase.h>
#include <windows.h>
#include <shellapi.h>
#include <iostream>

#include "dbg.h"
#include "Win32Exception.h"
#include "BmfWin32Utils.h"

namespace NasdaqProxy
{
	using namespace std;

	CWinService * CWinService::m_pServiceInstance = 0;
	_tstring CWinService::m_strDefaultEventSourceName = _T("");

	///	<summary>
	///		Constructor.
	/// </summary>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		02/08/2005 - CWin32Exception added
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	CWinService::CWinService(	const TCHAR* szServiceName, const TCHAR* szServiceDescription, const TCHAR* szDisplayName, 
								tagNTEventSourceInfo *pSourseInfo, DWORD dwControlsAccepted, DWORD dwCoInit):
		m_bDebugMode(false),
		m_bSilentMode(false),
		m_hThDebug(NULL),
		m_dwCurrentState(SERVICE_START_PENDING),
		m_dwControlsAccepted(dwControlsAccepted),
		m_dwConit(dwCoInit),
		m_NTEventSourseInfo(pSourseInfo!=NULL?*pSourseInfo:tagNTEventSourceInfo(/*default values*/)),
		m_hEvKillService(CreateEvent(NULL, TRUE, FALSE, NULL))
	{		
		ASSERT(NULL == m_pServiceInstance);
		ASSERT(NULL != m_hEvKillService);
		VERIFY_THROW_WIN32((NULL == m_hEvKillService), _T("CWinService::Run() - CreateEvent Failed"), GetLastError());
		
		// Dfine the Default Event source to use in static LogEvent in case we have many instances
		// of the service in a unique Machine and no instance is available 
		// (i.e., exception before we reach m_pServiceInstance = this or m_szServiceName not defined) (;
		m_strDefaultEventSourceName = m_NTEventSourseInfo.m_strDefaultEventSourceName;

		VERIFY_THROW_EXCEPTION((NULL != m_pServiceInstance), _T("CWinService::CWinService()"),  
			_T("Severe Error: only one instance of the Service is allowed!"), 0);

		SetServiceName(szServiceName, szServiceDescription, szDisplayName);
		SecureZeroMemory(m_szStartParams, sizeof(m_szStartParams));
		m_pServiceInstance = this;
		std::cout << "[CWinService] Service Name : " << szServiceName << std::endl;
	}

	///	<summary>
	///		Destructor.
	/// </summary>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		12/09/2000
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	CWinService::~CWinService()
	{
		if (NULL != m_hEvKillService) 
		{
			CloseHandle(m_hEvKillService);
			m_hEvKillService = NULL;
		}

		m_pServiceInstance = NULL; // don't delete it (MUST be a global object or a singleton)
	}


	///	<summary>
	///		Service startup.
	/// </summary>
	/// <returns>
	///		If no erros occurs, this method returns TRUE.
	/// </returns>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		17/05/2005 
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	BOOL CWinService::Startup()
	{
		BOOL bRet = ParseStartupCommands();
		if (FALSE == bRet)
		{
			std::cerr << _T("Error at Service Startup - Invalid arguments or combination of arguments") << std::endl;
			LogEvent(EVENTLOG_ERROR_TYPE, 0,ENVMSG_GENERIC, _T("Error at Service Startup - Invalid arguments or combination of arguments"));
		}
		return bRet;
	}

	BOOL CWinService::StartDispatcher()
	{
		// REGULAR MODE: The application is performed like a NT Service
			
		SERVICE_TABLE_ENTRY DispatchTable[] = 
		{
			{m_szServiceName, ServiceMain},			
			{NULL, NULL}
		};

		// Service startup.

		BOOL bRet = StartServiceCtrlDispatcher (DispatchTable);
		VERIFY_THROW_WIN32(!bRet, _T("CWinService::Start - StartServiceCtrlDispatcher"), GetLastError());

		return true;
	}

	BOOL CWinService::DebugService()
	{
		// DEBUG MODE: The application is not registered, and 
		// is started like a simple console application
		
		NasdaqProxy::EnableConsole( TRUE );

		LogEvent(EVENTLOG_INFORMATION_TYPE,0,EVMSG_STARTED,_T(""));
		
		std::cout << _T("  ---------------------------------------------   ") << std::endl;
		std::cout << _T("  |                                           |   ") << std::endl;
		std::cout << _T("  |               NASDAQ SERVICE              |   ") << std::endl;
		std::cout << _T("  |                                           |   ") << std::endl;
		std::cout << _T("  |               (DEBUG MODE)                |   ") << std::endl;
		std::cout << _T("  ---------------------------------------------   ") << std::endl;

		//std::cout << _T("*** Debug Mode ***") << std::endl;

		// Start a thread to monitor the keyboard. 
		// After a keystroke the service is finished.

		// Start the service

		if (!OnStart()) {
			std::cerr << _T("Error: Start request denied, see logs for more details") << std::endl;
			return FALSE;
			
		}
		
		m_dwCurrentState = SERVICE_RUNNING;

		// create a thread to wait until the user decides to quit
		unsigned nThreadId = 0;
		
		m_hThDebug = (HANDLE) _beginthreadex (NULL, 0, WaitForKeyboard, (LPVOID) this, 0, &nThreadId);

		
		// wait singnal the end of the thread
		WaitForSingleObject(m_hThDebug, INFINITE);
		
		if (m_hThDebug)  
		{
			CloseHandle(m_hThDebug);
			m_hThDebug = NULL;
		}
		
		// End the service
		OnStop();
		LogEvent(EVENTLOG_WARNING_TYPE,0,EVMSG_STOPPED,_T("Service stopped in debug mode"));
		return true;
	}

	///	<summary>
	///		Register the handle event function and call the client function
	///		OnStart()Service startup.
	/// </summary>
	/// <param name="dwArgc">
	///		Parameters count.
	/// </param>
	/// <param name="lpszArgv">
	///		Parameters list.
	/// </param>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		27/06/2001
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	void WINAPI CWinService::ServiceMain(DWORD dwArgc, LPTSTR* lpszArgv)
	{		
		ASSERT(NULL != m_pServiceInstance);
		m_pServiceInstance->m_serviceStatusHandle = RegisterServiceCtrlHandler(
															m_pServiceInstance->m_szServiceName,
															ServiceCtrlHandler);

		if (!m_pServiceInstance->m_serviceStatusHandle)
		{
			m_pServiceInstance->LogEvent(
										EVENTLOG_ERROR_TYPE,
										0,
										EVMSG_CTRLHANDLERNOTINSTALLED,
										GetWin32ErrorText(GetLastError()).c_str());
			return;
		}
		
		if (!m_pServiceInstance->SetStatus(SERVICE_START_PENDING))
		{
			return;
		}
		
		if (!m_pServiceInstance->OnStart())
		{
			m_pServiceInstance->LogEvent(
										EVENTLOG_ERROR_TYPE,
										0,
										EVMSG_FAILEDINIT,
										_T(""));
			return;
		}

		if (!m_pServiceInstance->SetStatus(SERVICE_RUNNING))
		{
			return;
		}
		
		// IMPORTANT: Run() blocks, when it returns it means the service has stopped.
		m_pServiceInstance->Run();
		// Notify that service stopped
		getServiceInstance()->LogEvent(EVENTLOG_WARNING_TYPE,0,EVMSG_STOPPED,_T("Service stopped"));
		
	}

	void CWinService::Run()
	{
		LogEvent(EVENTLOG_INFORMATION_TYPE,0,EVMSG_STARTED,_T("Service started"));
		ASSERT(NULL != m_hEvKillService);
		// waiting the signal to kill the service
		WaitForSingleObject(m_hEvKillService, INFINITE);
	}

	///	<summary>
	///		Event services controller.
	/// </summary>
	/// <param name="dwArgc">
	///		Event code.
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		20/06/2002
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	void WINAPI CWinService::ServiceCtrlHandler(DWORD dwOpcode)
	{
		ASSERT(NULL != m_pServiceInstance);
		switch (dwOpcode) 
		{
			case SERVICE_CONTROL_STOP: 
			case SERVICE_CONTROL_SHUTDOWN: 
				m_pServiceInstance->SetStatus (SERVICE_STOP_PENDING);
				m_pServiceInstance->OnStop();
				m_pServiceInstance->KillService();
				m_pServiceInstance->SetStatus(SERVICE_STOPPED);
				break;

			case SERVICE_CONTROL_PAUSE: 
				m_pServiceInstance->SetStatus (SERVICE_PAUSE_PENDING);
				m_pServiceInstance->OnPause();
				m_pServiceInstance->SetStatus (SERVICE_PAUSED);
				break;
				

			case SERVICE_CONTROL_CONTINUE: 
				m_pServiceInstance->SetStatus (SERVICE_CONTINUE_PENDING);
				m_pServiceInstance->OnContinue();
				m_pServiceInstance->SetStatus(SERVICE_RUNNING);
				break;

			case SERVICE_CONTROL_INTERROGATE: 			
				m_pServiceInstance->SetStatus(m_pServiceInstance->m_dwCurrentState);
				break;
			default:
				m_pServiceInstance->LogEvent(EVENTLOG_INFORMATION_TYPE,0,EVMSG_BADREQUEST,_T("Bad service "));
		}	
	}

	///	<summary>
	///		Set service status at SCM.
	/// </summary>
	/// <param name="dwState">
	///		Service states:
	///			- SERVICE_STOPPED
	///			- SERVICE_START_PENDING
	///			- SERVICE_STOP_PENDING
	///			- SERVICE_RUNNING
	///			- SERVICE_CONTINUE_PENDING
	///			- SERVICE_PAUSE_PENDING
	///			- SERVICE_PAUSED
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		22/11/2005 - if failed, log error and return false and treating SERVICE_START_PENDING situation
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	bool CWinService::SetStatus(
					DWORD dwState, 
					DWORD dwWin32ExitCode,
					DWORD dwServiceSpecificExitCode,
					DWORD dwCheckPoint,
					DWORD dwWaitHint)
	{
		SERVICE_STATUS ServiceStatus;

		ServiceStatus.dwServiceType				= SERVICE_WIN32_OWN_PROCESS;
		ServiceStatus.dwCurrentState			= dwState;
		
		if (dwState == SERVICE_START_PENDING)
			// service is in start-up mode, it won't accept STOP/PAUSE/CONTINUE/SHUTDOWN
			ServiceStatus.dwControlsAccepted= 0;
		else
			ServiceStatus.dwControlsAccepted = m_dwControlsAccepted;
		
		ServiceStatus.dwWin32ExitCode			= dwWin32ExitCode; 
		ServiceStatus.dwServiceSpecificExitCode	= dwServiceSpecificExitCode; 
		ServiceStatus.dwCheckPoint				= dwCheckPoint; 
		ServiceStatus.dwWaitHint				= dwWaitHint;

		m_dwCurrentState = dwState;

		if (!SetServiceStatus (m_serviceStatusHandle, &ServiceStatus)) 
		{
			m_pServiceInstance->LogEvent(
										EVENTLOG_ERROR_TYPE,
										0,
										EVMSG_CTRLHANDLERNOTINSTALLED,
										GetWin32ErrorText(GetLastError()).c_str());
			return false;
		}
		
		return true;
			
	}

	///	<summary>
	///		Keystroke monitor thread. After one keystroke, the service will
	///		be finished.
	/// </summary>
	/// <param name="lpParam">
	///		Reserved for future use, must be NULL.
	/// </param>
	/// <remarks>
	///		 This thread exist only on debug mode.
	/// </remarks>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		23/05/2005
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	UINT CWinService::WaitForKeyboard (LPVOID lpParam)
	{	
		ASSERT(NULL != m_pServiceInstance);
		while (m_pServiceInstance->m_dwCurrentState == SERVICE_RUNNING)
		{
			Sleep(1000);
					
			if (_kbhit())
			{			
				std::cout <<  _T("*** Service is shutting down ***") << std::endl;
				break;
			}			
		}
		m_pServiceInstance->m_dwCurrentState = SERVICE_STOPPED;
		return 0;
	}


	///	<summary>
	///		Parse command line arguments and call the especified function.
	/// </summary>
	/// <param name="argc">
	///		Arguments counts.
	/// </param>
	/// <param name="argv">
	///		Arguments list.
	/// </param>
	/// <remarks>
	///		 There are three valid arguments:
	///			-d: Debug mode
	///			-i: Install service on SCM
	///			-r: Remove service from SCM
	/// </remarks>
	///	<creation>
	///		12/09/2000
	/// </creation>
	/// <modification>
	///		12/09/2000
	///	</modification>
	/// <author>
	///		Rafael Lucyk / Djalma (some improvements)
	/// </author>

	BOOL CWinService::ParseStartupCommands()
	{
		BOOL bQuit = FALSE;
		BOOL (CWinService::* pfnc)() = NULL;
	#ifdef _CONSOLE
		const int iStartIndex = 1; // ignore the program name
	#else
		const int iStartIndex = 0;
	#endif
		for (int iCurIdx = iStartIndex; iCurIdx < __argc; iCurIdx++)
		{	
			// Debug mode
			if (!lstrcmpi (__argv[iCurIdx], _T("-d")))			
			{				
				if (NULL != pfnc)
					return FALSE;
				
				m_bDebugMode = true;
				pfnc = &CWinService::DebugService;				
			}

			// Service installation on SCM
			else if (!lstrcmpi (__argv[iCurIdx], _T("-i")))		
			{			
				if (NULL != pfnc)
					return FALSE;
				
				pfnc = &CWinService::InstallService;
				std::cout << " INSTALL SERVICE \r\n";
			}
			
			// Remove service from SCM
			else if (!lstrcmpi (__argv[iCurIdx], _T("-r")))
			{
				if (NULL != pfnc)
					return FALSE;
				
				pfnc = &CWinService::DeleteService;
				std::cout << " REMOVE SERVICE \r\n";
			}
			// Silent
			else if (!lstrcmpi (__argv[iCurIdx], _T("-s"))) 
			{
				m_bSilentMode = true;
			}
			// Help
			else if (!lstrcmpi (__argv[iCurIdx], _T("-?")))
			{
				if (NULL != pfnc)
					return FALSE;

				pfnc = &CWinService::Usage;
			}
			// Additional command 
			else if (__argv[iCurIdx][0] == '-')
			{
				// call virtual function to parse additional command argument in derived class
				
				LPTSTR pszExtraInfo = NULL;
				// additional info may exist in next arg, if not another commnad '-'
				if ((iCurIdx+1 < __argc) && (__argv[iCurIdx+1][0] != '-')) // check if not another command
				{
					pszExtraInfo = __argv[iCurIdx+1];
				}
				
				// command will be passed to derived class
				bQuit = OnCustomStartupCommand(__argv[iCurIdx], pszExtraInfo);
			}

		}

		if ((NULL == pfnc) && !bQuit)
		{
			pfnc = &CWinService::StartDispatcher;
		}
		
		// call the selected function
		if(pfnc != NULL)
			(this->*pfnc)();
		
		return TRUE;
	}


	///	<summary>
	///		Install the service on SCM.
	/// </summary>
	/// <returns>
	///		This method returns true is the installation finished with success and false
	///		if installation failed.
	/// </returns>
	///	<creation>
	///		10/03/1999
	/// </creation>
	/// <modification>
	///		10/03/1999
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	BOOL CWinService::InstallService()
	{
		BOOL		bRet = FALSE;
		SC_HANDLE	hSCM = NULL,
					hService = NULL;

		hSCM = OpenSCManager(NULL, SERVICES_ACTIVE_DATABASE, SC_MANAGER_CREATE_SERVICE);
		if (!hSCM)
		{
			if (!m_bSilentMode)
			{
				std::stringstream errText;
				errText << _T("Service ") << m_szServiceName;
				errText << _T(" could not be installed because OpenSCManager failed") << std::endl; 
				errText << GetWin32ErrorText(GetLastError());
				ShowMessage(errText.str().c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			}
			return FALSE;
		}

		if ((NULL == m_szServiceName) || (_tcscmp(m_szServiceName,_T("")) == 0)) {
			if (!m_bSilentMode)
				ShowMessage(_T("Installation failure, no service name found"), _T("Error"), MB_OK|MB_ICONERROR);
			return FALSE;
		}
		
		TCHAR szFilePath[MAX_PATH];
		GetModuleFileName(NULL, szFilePath, sizeof(szFilePath)/sizeof(TCHAR));
		if ((0 != m_szStartParams) && ( lstrcmp(m_szStartParams, _T("")) != 0)) 
		{
			const std::size_t maxSize = sizeof(szFilePath) / sizeof(TCHAR) - 1;
			_tcsncat(szFilePath, _T(" "), __min(1, maxSize-_tcslen(szFilePath)));
			_tcsncat(szFilePath, m_szStartParams, __min(_tcslen(m_szStartParams), maxSize-_tcslen(szFilePath)));
		}

		hService = CreateService (hSCM,
									m_szServiceName,
									m_szDisplayName,
									SERVICE_ALL_ACCESS,
									SERVICE_WIN32_OWN_PROCESS,
									SERVICE_DEMAND_START,
									SERVICE_ERROR_NORMAL,
									szFilePath,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL);

		if (hService != NULL)
		{
			SERVICE_DESCRIPTION sdBuf;
			sdBuf.lpDescription = m_szServiceDescription;
			ChangeServiceConfig2(hService, SERVICE_CONFIG_DESCRIPTION, &sdBuf);

			CloseServiceHandle(hService);
			// Add event source with same name of the Service in order to use LogEvent
			AddEventSource(m_szServiceName,
							m_NTEventSourseInfo.m_dwCategoryCount,
							m_NTEventSourseInfo.m_bSupportParameterMsgFile);
			// Add Default event source name in order to use when there is no instance
			if (!m_strDefaultEventSourceName.empty())
			{
				AddEventSource(m_strDefaultEventSourceName.c_str(),
								m_NTEventSourseInfo.m_dwCategoryCount,
								m_NTEventSourseInfo.m_bSupportParameterMsgFile);
			}

			LogEvent(EVENTLOG_INFORMATION_TYPE,0,EVMSG_INSTALLED,m_szServiceName);

			if (!m_bSilentMode)
			{
				std::stringstream text;
				text << _T("Service ") << m_szServiceName << " has been installed." << std::endl;
				ShowMessage(text.str().c_str(), _T("Information"), MB_OK|MB_ICONINFORMATION);
			}
			bRet = TRUE;
		}
		else 
		{
			if (!m_bSilentMode)
				ShowMessage(GetWin32ErrorText(::GetLastError()).c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			bRet = FALSE;
		}

		CloseServiceHandle(hSCM);

		return bRet;
	}

	///	<summary>
	///		Remove the service from SCM.
	/// </summary>
	/// <returns>
	///		This method returns true is the service is removed from SCM and false
	///		if removal failed.
	/// </returns>
	///	<creation>
	///		10/03/1999
	/// </creation>
	/// <modification>
	///		10/03/1999
	///	</modification>
	/// <author>
	///		Rafael Lucyk
	/// </author>

	BOOL CWinService::DeleteService()
	{
		BOOL		bRet = false;
		SC_HANDLE	hSCM = NULL,
					hService = NULL;
		
		hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

		if (!hSCM)
		{
			if (!m_bSilentMode)
				ShowMessage(GetWin32ErrorText(GetLastError()).c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			return FALSE;
		}

		hService = OpenService(hSCM, m_szServiceName, SERVICE_ALL_ACCESS | DELETE);

		if (!hService)
		{
			if (!m_bSilentMode)
				ShowMessage(GetWin32ErrorText(GetLastError()).c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			return FALSE;
		}
		
		
		// Stop the service if necessary
		SERVICE_STATUS	ServiceStatus;
		BOOL bSuccess =	QueryServiceStatus(hService, &ServiceStatus);
		if (!bSuccess)
		{
			if (!m_bSilentMode)
				ShowMessage(GetWin32ErrorText(GetLastError()).c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			return FALSE;
		}

		if (ServiceStatus.dwCurrentState != SERVICE_STOPPED)
		{
			std::cout << _T("Service currently active.  Stopping service...") << std::endl;
			std::cout << _T("Stopping ") <<  m_szServiceName << " ..." << std::endl;

			bSuccess = ControlService(hService, SERVICE_CONTROL_STOP, &ServiceStatus);
			if (!bSuccess)
			{
				if(!m_bSilentMode)
				{
					std::stringstream text;
					text << _T("Failed attempting to stop the ")  << m_szServiceName << _T(" Service") << std::endl;
					text << GetWin32ErrorText(::GetLastError());
					ShowMessage(text.str().c_str(), _T("Error"), MB_OK|MB_ICONERROR);
				}
				return FALSE;
			}
	      
			while( QueryServiceStatus(hService, &ServiceStatus) ) 
			{
				
				if( ServiceStatus.dwCurrentState == SERVICE_STOP_PENDING ) 
				{
					std::cout << _T(".");
					Sleep(10);
				} 
				else
					break;
			}

			if( ServiceStatus.dwCurrentState == SERVICE_STOPPED )
				std::cout << std::endl << m_szServiceName << _T(" stopped.") << std::endl;
			else
			{
				if (!m_bSilentMode)
					ShowMessage(_T("Failed to stop Service"), _T("Error"), MB_OK|MB_ICONERROR);
				return FALSE;
			}
		}
		
		if (::DeleteService(hService)) 
		{
			LogEvent(EVENTLOG_WARNING_TYPE,0,EVMSG_REMOVED,m_szServiceName);
			if (!m_bSilentMode)
			{
				std::stringstream text;
				text << _T("Service ") << m_szServiceName << _T(" was removed.");
				ShowMessage(text.str().c_str(), _T("Warning"), MB_OK|MB_ICONWARNING);
			}
			bRet = TRUE;
		}
		else
		{
			if (!m_bSilentMode)
				ShowMessage(GetWin32ErrorText(GetLastError()).c_str(), _T("Error"), MB_OK|MB_ICONERROR);
			bRet = FALSE;
		}
		CloseServiceHandle(hService);			
		CloseServiceHandle(hSCM);
		return bRet;
	}

	bool CWinService::IsInstalled() const
	{
		bool		bRet = false;
		SC_HANDLE	hSCM = NULL,
					hService = NULL;
		
		hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

		if (hSCM != NULL)
		{
			hService = OpenService(hSCM, m_szServiceName, SERVICE_QUERY_CONFIG);

			if (hService != NULL)
			{
				bRet = true;

				CloseServiceHandle(hService);			
			}
			
			CloseServiceHandle(hSCM);
		}

		return bRet;
	}


	// in certain conditions one thread might want to stop the service
	// use with care

	void CWinService::HaltService()
	{
		
		if (!IsRunning()) return;
		
		if (!StopService()) exit(-1);
		/*
		if ( IsDebugMode() ) 
			StopService(); // safe in debug mode
		else
		{
			ServiceCtrlHandler( SERVICE_CONTROL_STOP );
			//KillService();
		}
		*/
	}

	bool CWinService::StopService()
	{
		if (m_bDebugMode)
		{
			if (m_hThDebug) 
			{
				m_dwCurrentState = SERVICE_STOP_PENDING;
				ASSERT(::WaitForSingleObject(m_hThDebug, 5000) != WAIT_TIMEOUT);
			}
			return true;
		}

		// trying to use minimum security access

		SC_HANDLE hSCM, hService;
		BOOL bSuccess;
		SERVICE_STATUS status;
	  
		// Open a Service Control Manager connection
		hSCM = OpenSCManager(0, 0, SC_MANAGER_CONNECT );
		if (!hSCM)
		{
			//HandleError("OpenSCManager Fails!", GetLastError());
			LogEvent(EVENTLOG_ERROR_TYPE,0,ENVMSG_GENERIC,GetWin32ErrorText(GetLastError()).c_str());
			return false;
		}
		
		// Get the service's handle
		hService = OpenService(hSCM, m_szServiceName, SERVICE_QUERY_STATUS | SERVICE_STOP );
		if (!hService)
		{
			LogEvent(EVENTLOG_ERROR_TYPE,0,ENVMSG_GENERIC,GetWin32ErrorText(GetLastError()).c_str());
			return false;
		}
		// Stop the service if necessary
		bSuccess = QueryServiceStatus(hService, &status);
		if (!bSuccess)
		{
			LogEvent(EVENTLOG_ERROR_TYPE,0,ENVMSG_GENERIC,GetWin32ErrorText(GetLastError()).c_str());
			return false;
		}
		if (status.dwCurrentState != SERVICE_STOPPED)
		{
			// Service currently active,  stopping service...
			bSuccess = ControlService(hService, SERVICE_CONTROL_STOP, &status);
			if (!bSuccess)
			{
				LogEvent(EVENTLOG_ERROR_TYPE,0,ENVMSG_GENERIC,GetWin32ErrorText(GetLastError()).c_str());
				return false;
			}
		}
		return true;
	 }

	void CWinService::SetServiceName(const TCHAR *szServiceName, const TCHAR *szServiceDescription, const TCHAR *szDisplayName)
	{
		VERIFY_THROW_EXCEPTION(((NULL == szServiceName) || (NULL == szDisplayName) || (NULL == szServiceDescription)
			|| (strcmp(szServiceName, _T("")) == 0)
			|| (strcmp(szServiceDescription, _T("")) == 0)
			|| (strcmp(szDisplayName, _T("")) == 0)), _T("CWinService::SetServiceName"), _T("Invalid service name defined"), 0);
		_tcsncpy (m_szServiceName, szServiceName, sizeof(m_szServiceName) / sizeof(TCHAR) - 1);
		_tcsncpy (m_szServiceDescription, szServiceDescription, sizeof(m_szServiceDescription) / sizeof(TCHAR) - 1);
		_tcsncpy (m_szDisplayName, szDisplayName, sizeof(m_szDisplayName) / sizeof(TCHAR) - 1);
	}

	void CWinService::AddStartParam(const TCHAR* szParam)
	{
		VERIFY_THROW_EXCEPTION(((NULL == szParam) || (strcmp(szParam, _T("")) == 0)), _T("Service Startup Parameters"), _T("Unexpected error - invalid startup parameters"), 0);
		if ( 0 != m_szStartParams ) 
		{
			const std::size_t maxSize = sizeof(m_szStartParams) / sizeof(TCHAR) - 1;
			if ( lstrcmp(m_szStartParams, _T("")) == 0 ) 
			{ 
				_tcsncpy (m_szStartParams, szParam, sizeof(m_szStartParams) / sizeof(TCHAR) - 1);
			}
			else 
		{
				_tcsncat(m_szStartParams, _T(" "), __min(1, maxSize-_tcslen(m_szStartParams)));
				_tcsncat(m_szStartParams, szParam, __min(_tcslen(szParam), maxSize-_tcslen(m_szStartParams)));
			}
		}
	}

	void CWinService::LogEvent(WORD wType, WORD wCategory, DWORD dwID,LPCTSTR pFormat,...)
	{
		if ((NULL != m_pServiceInstance) && (strcmp(m_pServiceInstance->m_szServiceName,_T(""))!=0))
			NasdaqProxy::CEventLogging::LogEvent(m_pServiceInstance->m_szServiceName, wType, wCategory, dwID, pFormat);
		else 
			if (!m_strDefaultEventSourceName.empty())
				NasdaqProxy::CEventLogging::LogEvent(m_strDefaultEventSourceName.c_str(), wType, wCategory, dwID, pFormat);
	}

	void CWinService::AddEventSource(const TCHAR* cszEventSourceName, DWORD dwCategoryCount, bool bSupportParameterMessageFile)
	{
		if (!m_NTEventSourseInfo.m_bUseExternalPE4MessageResource)
		{
			// Name of the PE module that contains the message resource (the EXE itself)
			// this should be used only when the EXE contains the message resources
			TCHAR szPath[ MAX_PATH ];
			::GetModuleFileName( NULL, szPath, MAX_PATH );
			NasdaqProxy::AddEventSource(cszEventSourceName, szPath, dwCategoryCount, bSupportParameterMessageFile);
		}
		else
		{
			// An external DLL shall be used
			// CEventLogging encapsulates the complexity of finding the path of the DLL
				NasdaqProxy::CEventLogging::AddEventSource(cszEventSourceName, dwCategoryCount, bSupportParameterMessageFile, m_dwConit);
		}
	}

	//
	// Removes the passed source of events from the registry
	//
	void CWinService::RemoveEventSource()
	{
		NasdaqProxy::CEventLogging::RemoveEventSource( m_szServiceName );
	}

	BOOL CWinService::Usage()
	{
		TCHAR szMsg[_MAX_PATH] = {0};
	#if defined(_MSC_VER) && (_MSC_VER >= 1400)
		sprintf_s(szMsg, _MAX_PATH, SVR_MSG, __argv[0]);	
	#else
		sprintf(szMsg, SVR_MSG, __argv[0]);
	#endif
		MessageBox(NULL, szMsg, "FixGateway", MB_OK|MB_ICONWARNING);
		return TRUE;
	}


} // namespace NasdaqProxy