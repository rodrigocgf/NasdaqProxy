// VersionInfo.cpp: Implementation of CVersionInfo.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "stdstring.h"
#include "VersionInfo.h"
//#include <windows.h>

namespace NasdaqProxy 
{

	//-----------------------------------------------------------------------------
	// Function name	: CVersionInfo::GetVersionInfo
	// Description	    : 
	// Author			: Djalma
	// History			: 
	// Return type		: CVersionInfo::VersionInfo 
	// Argument         : tstring szFullPath
	// Argument         : WORD wLanguage
	//-----------------------------------------------------------------------------
	CVersionInfo::VersionInfo CVersionInfo::GetVersionInfo(tstring szFullPath,WORD wLanguage)
	{

		BOOL		bResult ;						// Result of Boolean functions
		DWORD		dwVerInfoSize ;					// Size of version information
		DWORD		dwHandle ;						// Extraneous but required parameter
		LPVOID		pVerInfo ;						// File version info pointer
		LPVOID		pValue ;						// Value from version info
		TCHAR		szCompFullPath[MAX_PATH]={0};	// Application executable path
		UINT		uLength ;						// Length of retrieved value
		WORD		wCodePage = ::GetACP();

		VersionInfo myInfo;

		_tcscpy( szCompFullPath, szFullPath.c_str());

		// Determine the size buffer needed to store the version information:
		dwVerInfoSize = ::GetFileVersionInfoSize (szCompFullPath, &dwHandle) ;
		if (dwVerInfoSize)
		{
		  // Allocate a buffer for the version info block
			pVerInfo = new char [dwVerInfoSize] ;
			if(pVerInfo)
			{
				UINT uLen;

				// Read the version info block into the buffer
				::GetFileVersionInfo (szCompFullPath, dwHandle, dwVerInfoSize, pVerInfo) ;

				// search for language
				struct LANGANDCODEPAGE
				{
					WORD wLanguage;
					WORD wCodePage;
				} *lpTranslate;
				::VerQueryValue((LPVOID)pVerInfo, _T("\\VarFileInfo\\Translation"), (LPVOID*)&lpTranslate, &uLen);
				
				LONG nCount = uLen / sizeof(struct LANGANDCODEPAGE);
				LONG n = 0;
				for (; n < nCount; n++)
				{
					if (lpTranslate[n].wLanguage == wLanguage && lpTranslate[n].wCodePage == wCodePage)
						break;
				}

				n = n < nCount ? n : 0L;
				TCHAR strValue[2*MAX_PATH] = {0};

				//
				// Getting product name
				//
				_stprintf(strValue, _T("\\StringFileInfo\\%04x%04x\\ProductName"), lpTranslate[n].wLanguage, lpTranslate[n].wCodePage);

				// Retrieve the value
				bResult = ::VerQueryValue (pVerInfo,strValue, &pValue, &uLength) ;
				if(bResult)
				{
					TCHAR	szProductName[MAX_PATH]={0};	// buffer for ProductName result			
					_tcsncpy(szProductName,(LPCTSTR)pValue,uLength);

					myInfo.strProductName = szProductName;
				}

				//
				// Getting product version
				//
				_stprintf(strValue,_T("\\StringFileInfo\\%04x%04x\\ProductVersion"), lpTranslate[n].wLanguage, lpTranslate[n].wCodePage);

				// Retrieve the value
				bResult = ::VerQueryValue (pVerInfo, (LPTSTR)(LPCTSTR)strValue, &pValue, &uLength) ;
				if(bResult)
				{
					TCHAR		szProductVersion[MAX_PATH]={0};	// buffer for ProductVersion result
					_tcsncpy(szProductVersion,(LPCTSTR)pValue,uLength);
					myInfo.strProductVersion = szProductVersion;
				}


				//
				// Getting file version
				//
				_stprintf(strValue,_T("\\StringFileInfo\\%04x%04x\\FileVersion"), lpTranslate[n].wLanguage, lpTranslate[n].wCodePage);

				// Retrieve the value
				bResult = ::VerQueryValue (pVerInfo, (LPTSTR)(LPCTSTR)strValue, &pValue, &uLength) ;
				if(bResult)
				{
					TCHAR	szFileVersion[MAX_PATH]={0};	
					_tcsncpy(szFileVersion,(LPCTSTR)pValue,uLength);
					myInfo.strFileVersion = szFileVersion;
				}


				///////////////////////////////////////////////////////////////////
				//
				//	Getting Copyright info
				//

				_stprintf(strValue,_T("\\StringFileInfo\\%04x%04x\\LegalCopyright"), lpTranslate[n].wLanguage, lpTranslate[n].wCodePage);

				// Retrieve the value
				if( ::VerQueryValue (pVerInfo, (LPTSTR)(LPCTSTR)strValue, &pValue, &uLength) )
				{
					TCHAR		szCopyR[MAX_PATH]={0};	
					_tcsncpy(szCopyR,(LPCTSTR)pValue,uLength);
					myInfo.strCopyright = szCopyR;
				}

				///////////////////////////////////////////////////////////////////
				//
				//	Getting Comment info
				//
				_stprintf(strValue,_T("\\StringFileInfo\\%04x%04x\\Comments"),	lpTranslate[n].wLanguage, lpTranslate[n].wCodePage);
			
				// Retrieve the value
				if( ::VerQueryValue (pVerInfo, (LPTSTR)(LPCTSTR)strValue, &pValue, &uLength) )
				{
					TCHAR	szComments[MAX_PATH]={0};
					_tcsncpy(szComments,(LPCTSTR)pValue,uLength);
					myInfo.strComments = szComments;
				}

				// Format the output for the dialog caption
				// Get the current caption then append to it the ProductName value

				// Free the memory for the version information block
				delete [] pVerInfo ;
			}
		}

		return myInfo;
	}

} 
