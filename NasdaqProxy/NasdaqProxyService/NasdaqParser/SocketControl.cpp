/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: SocketControl.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a	
	Date	: june 9 , 2010
=========================================================

*/
#include "StdAfx.h"
#include "SocketControl.h"
#include "SoupBinTCP.h"
#include "Logger.h"

#define	PARENT ((SoupBinTCP *)m_ParentPtr)

namespace NasdaqProxy 
{
	using boost::mutex;		

	SocketControl::SocketControl(void * pParent, Config lConfig) : timer_conn(this,&SocketControl::TimerEventCallback,TID_RECONN) , m_Config(lConfig)
	{		
		Logger::logtrc('C',"SocketControl::SocketControl() Init");
		
		timer_conn.set_interval(3000);		
		
		m_connectionSocket = INVALID_SOCKET;		
		
		b_loopWait = true;
		m_ParentPtr = pParent;

		InitializeStateMachine();
		ConfigureStateMachine();		
		
	}

	SocketControl::~SocketControl(void)
	{			
		Logger::logtrc('C',"SocketControl::~SocketControl()");		
	}

	void SocketControl::Start()
	{
		m_intEventSocket = 0;
		_socketThreadPtr.reset ( new boost::thread ( boost::bind ( &SocketControl::MainThreadLoop , this ) ) );
		m_event = ONSTART;
	}

	void SocketControl::Stop()
	{		
		Logger::logtrc('C',"SocketControl::Stop() Init");

		timer_conn.Destroy();		
		m_intEventSocket = 1;		

		_socketThreadPtr->join();

		SetOutEvent(_ON_IDLE_);		
		
		Logger::logtrc('C',"SocketControl::Stop() Finish");
	}

	void SocketControl::InitializeStateMachine()
    {	
		for (int i = 0; i < (int)SIZEOF_STATES; i++)
        {
            for (int j = 0; j < (int) SIZEOF_EVENTS; j++)
            {
				_stateMachineSocket[i][j] = st_STATE_MACHINE(SOCKETCONTROL::REMAIN, &SocketControl::nop );
            }
        }
    }

	bool SocketControl::DoWait()
	{
		bool bRet = false;
		
		if ( b_loopWait )
		{
			boost::xtime_get(&m_waitInterval, boost::TIME_UTC);
			m_waitInterval.nsec += 1*1000; // 1 us
			boost::thread::sleep(m_waitInterval);
		}

		if ( m_intEventSocket == 1 )
			bRet = true;		

		return bRet;
	}

	void SocketControl::MainThreadLoop()
	{
		int actionEvent = SOCKETCONTROL::NO_EVENT;	
		
		while ( !DoWait())
		{
			m_state = _currentState;

			if (m_event != SOCKETCONTROL::NO_EVENT)
			{
				if (_stateMachineSocket[_currentState][m_event].i_Proximo_Estado != SOCKETCONTROL::REMAIN)
				{
					_currentState = _stateMachineSocket[_currentState][ m_event].i_Proximo_Estado;
				}
				actionEvent = m_event;

				m_event = SOCKETCONTROL::NO_EVENT;

				(this->*_stateMachineSocket[m_state][actionEvent].acao)();
			}

			MonitorEvents();
		}	
		
		Logger::logtrc('C',"SocketControl::MainThreadLoop Finish");
	}

	void SocketControl::MonitorEvents()
	{
		int bytesToReceive = 0;		
		b_loopWait = true;

		if ( m_IncomingMessageQueue.size() > 0 )
		{
			int iEvent = GetInEvent();
			
			if ( iEvent == _STOP_ ) 
			{			
				m_event = ONSTOP;
				return;
			}
			else if ( iEvent == _DISCONNECT_ ) 
			{
				m_event = ONDISCONNECT;
				Logger::logtrc('C',"[SOCKET] EVENT ONDISCONNECT");
				return;
			} 
		}

		if ( m_state == CONNECTED )
		{
			if ( WaitToReceive() )
			{				
				b_loopWait = false;				
				SetOutEvent(_SOCKET_DATA_);				
			}
		}
	}

	
	int SocketControl::WaitToReceive()
	{
		FD_SET		fdsReceive;
		FD_SET		fdsExcept;
		TIMEVAL		tvReceive = {0, 0};
		int			nSockets;
		
		mError = 0;
		
		FD_ZERO(&fdsReceive);
		FD_ZERO(&fdsExcept);
		FD_SET(m_connectionSocket, &fdsReceive);
		FD_SET(m_connectionSocket, &fdsExcept);		
		
		nSockets = select(NULL, &fdsReceive, NULL, &fdsExcept, &tvReceive);		
		
		if (nSockets == 0) 
		{
			mError = WSAETIMEDOUT;
			return 0;
		}

		if( (nSockets == SOCKET_ERROR) || FD_ISSET(m_connectionSocket, &fdsExcept) )
		{
			mError = WSAGetLastError();	
			Logger::logerr('C',"[SOCKET] Error at WaitToReceive : %d", mError);
			m_event = ONERROR;
			SetOutEvent(_SOCKET_ERROR_);
			return 0;
		}

		return 1;
		
	}

	unsigned long	 SocketControl::GetSockAvailableBytes()
	{
		unsigned long	BytesToRead = 0;
		ioctlsocket( m_connectionSocket , FIONREAD, &BytesToRead );

		return BytesToRead;

	}

	int SocketControl::Receive(unsigned char *Buffer, int	BytesToRead)
	{
		int				BytesRecebidos;		
		int				TotalRecebido = 0;
		DWORD			dw_res;				
		
		while(TotalRecebido < BytesToRead) 
		{			
			
			BytesRecebidos = recv(	m_connectionSocket , (char *)&Buffer[TotalRecebido], (BytesToRead - TotalRecebido) ,0);
			
			if ( BytesRecebidos == 0 ) 
			{				
				mError = WSAEINVAL;
				m_event = ONERROR;				
				SetOutEvent(_SOCKET_ERROR_);
				return -1;
			}

			if ( WSAGetLastError() == WSAEINVAL ) 
			{				
				mError = WSAEINVAL;
				m_event = ONERROR;				
				SetOutEvent(_SOCKET_ERROR_);
				Logger::logerr('C',"[SOCKET] Error at Receive : %d", mError);
				return -1;
			}

			if ( BytesRecebidos == SOCKET_ERROR ) 
			{			
				dw_res = WSAGetLastError();
				
				if ( dw_res != WSAEWOULDBLOCK ) 
				{					
					m_event = ONERROR;					
					SetOutEvent(_SOCKET_ERROR_);
					Logger::logerr('C',"[SOCKET] Error at Receive : %d", dw_res);

					return BytesRecebidos;
				}
			}
			
			TotalRecebido += BytesRecebidos;
		}

		return TotalRecebido;
	}

	
	int SocketControl::Send(unsigned char * p_buffer, int BytesToSend)
	{
		int		BytesSent;
		int		TotalSent = 0;
		DWORD   dw_res;
		
		while ( TotalSent < BytesToSend )
		{
			BytesSent = send ( m_connectionSocket, (char *)&p_buffer[TotalSent], (BytesToSend - TotalSent), 0);

			if ( BytesSent == 0 )
			{
				mError = WSAEINVAL;
				m_event = ONERROR;				
				SetOutEvent(_SOCKET_ERROR_);
				Logger::logerr('C',"[SOCKET] Error at Send : %d", mError);

				return -1;
			}

			if ( WSAGetLastError() == WSAEINVAL ) 
			{				
				mError = WSAEINVAL;
				m_event = ONERROR;				
				SetOutEvent(_SOCKET_ERROR_);
				Logger::logerr('C',"[SOCKET] Error at Send : %d", mError);

				return -1;
			}

			if ( BytesSent == SOCKET_ERROR ) 
			{			
				dw_res = WSAGetLastError();
				
				if ( dw_res != WSAEWOULDBLOCK ) 
				{					
					m_event = ONERROR;					
					SetOutEvent(_SOCKET_ERROR_);
					Logger::logerr('C',"[SOCKET] Error at Send : %d", dw_res);

					return BytesSent;
				}
			}
			
			TotalSent += BytesSent;
		}

		return TotalSent;		
	}


	/// <summary>
	///	PUSH FRONT
	/// </summary>
	void SocketControl::SetOutEvent(int iEvent)
	{
		
		boost::mutex::scoped_lock lockKeeper(PARENT->m_IncommingQueueLock);

		if ( PARENT->GetLastEvent() != iEvent )
			PARENT->m_IncomingMessageQueue.push_front(iEvent);
		
	}

	/// <summary>
	///	GET BACK
	/// </summary>
	int SocketControl::GetInEvent()
	{
		int iEvent = 0;

		boost::mutex::scoped_lock lockKeeper(m_IncommingQueueLock);

		iEvent = m_IncomingMessageQueue.back();
		m_IncomingMessageQueue.pop_back();

		return iEvent;
	}
	

	void SocketControl::LogStateMachine()
	{
		/*
		
		
                                      INFINITE CONNECTION ATTEMPTS SOCKET AUTOMATON                                        
                                      ---------------------------------------------                                        
                                                                                                                           
                                -----------------                                                                          
                                |               |              ONSTOP /                                                    
           -------------------->|    IDLE       |<---------------------------------------------------                      
           |                    |               |            DisconnectDestroyTimer()               |                      
           |                    -----------------                                                   |                      
           |                            |                                                           |                      
           |                   ONSTART  | / Connect()       ONERROR /                               |                      
           |                            |               CreateTimer(TID_RECONN)                     |                      
           |                            |         ___________________                               |                      
           |                            v        /                  _\\|                             |                     
           |                    -----------------                    ----------------------         |                      
           |   ONSTOP           |               |                    |                    |         |                      
           |<-------------------| TRY CONNECT   |                    |  WAIT TO RECONNECT |----------                      
           |                    |               |                    |                    |                                
           |                    ---------------- _   ONRECONNTOUT    ----------------------                                
           |                          |         |\\___________________/            ^                                       
           |                          |                  /                        |                                        
           |               ONCONNECT  |         ConnectDestroyTimer()     ONERROR | / CreateTimer(TID_RECONN)   
           |                   /      |                                           |                                        
           |          ConnectionOK()  v                                           |                                        
           |                    -----------------                                 |                                        
           |   ONSTOP           |               |                                 |                                        
           |--------------------|   CONNECTED   |_________________________________|                                        
                                |               |                                                            
               Disconnect()     -----------------                                                                         
                                     
									 
									  
									  
		*/
	}

	void SocketControl::ConfigureStateMachine()
    {            

        /*                    CURRENT STATE             EVENT 	                            NEXT STATE                      ACTION      */

		_stateMachineSocket[       IDLE       ][     ONSTART      ] = st_STATE_MACHINE(   TRY_CONNECT     ,         &SocketControl::Connect       );

		_stateMachineSocket[   TRY_CONNECT    ][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::Disconnect    );
		_stateMachineSocket[   TRY_CONNECT    ][     ONCONNECT    ] = st_STATE_MACHINE(     CONNECTED     ,         &SocketControl::ConnectionOK  );
		_stateMachineSocket[   TRY_CONNECT    ][     ONERROR      ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::CreateTimer   );

		_stateMachineSocket[ WAIT_TO_RECONNECT][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::DisconnectDestroyTimer   );
		_stateMachineSocket[ WAIT_TO_RECONNECT][   ONRECONNTOUT   ] = st_STATE_MACHINE(  TRY_CONNECT      ,         &SocketControl::ConnectDestroyTimer   );
		_stateMachineSocket[ WAIT_TO_RECONNECT][     ONERROR      ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::CreateTimer   );


		_stateMachineSocket[    CONNECTED     ][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::Disconnect   );
		_stateMachineSocket[    CONNECTED     ][     ONERROR      ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::CreateTimer   );		
		_stateMachineSocket[    CONNECTED     ][   ONDISCONNECT   ] = st_STATE_MACHINE(    TRY_CONNECT    ,         &SocketControl::Reconnect );		

        _currentState = (int)IDLE;
		m_event = (int)SOCKETCONTROL::NO_EVENT;
    }

	void SocketControl::nop()
	{

	}

	void SocketControl::Connect()
	{
		Logger::logtrc('C',"[SOCKET] Connect()");
		HOSTENT * host;

		memset( (void*)&m_SockAddr, 0, sizeof( m_SockAddr ) );
		m_connectionSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);	 
		DWORD dwReUse = 1;	

		if(m_connectionSocket != INVALID_SOCKET) 
		{
			m_SockAddr.sin_family = AF_INET;			
			m_SockAddr.sin_port = htons(atoi(m_Config.m_strNasdaqTcpPort.c_str() ) );
			
			if((m_SockAddr.sin_addr.s_addr = inet_addr( m_Config.m_strNasdaqIpAddress.c_str() ) ) == INADDR_NONE)			
			{				
				if( host = (HOSTENT*)gethostbyname(m_Config.m_strNasdaqIpAddress.c_str() ) )				
				{
					memcpy((void*)&m_SockAddr.sin_addr, (void*)host->h_addr_list[0], host->h_length);
				}
				else
				{					
					closesocket(m_connectionSocket);
					m_connectionSocket = 0;				
					m_event = ONERROR;
					Logger::logerr('C',"[SOCKET] EVENT ONERROR");
				}
			}

			if(connect(m_connectionSocket, (const struct sockaddr*)&m_SockAddr, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
			{
				DWORD dwError = WSAGetLastError();
				closesocket(m_connectionSocket);
				m_connectionSocket = 0;		

				Logger::logerr('C',"[SOCKET] EVENT ONERROR");
				m_event = ONERROR;
			} else {
				m_event = ONCONNECT;
				Logger::logtrc('C',"[SOCKET] EVENT ONCONNECT");
			}
		} else {
			Logger::logerr('C',"[SOCKET] Invalid Socket. Error : %d",WSAGetLastError());
		}
	}

	///<summary>
	///	Tells Upper state machine about Connection event
	/// and waits for upper event queue processing.
	///</summary>
	void SocketControl::ConnectionOK()
	{
		Logger::logtrc('C',"[SOCKET] ConnectionOK()");
		//SetOutEvent("EVT_CONNECTED");		
		SetOutEvent(_CONNECTED_);		

		boost::xtime			delay;
		boost::xtime_get(&delay, boost::TIME_UTC);
		delay.nsec += 5*1000; 
		boost::thread::sleep(delay);
	}

	void SocketControl::Disconnect()
	{
		Logger::logtrc('C',"[SOCKET] Disconnect()");
		if(m_connectionSocket != INVALID_SOCKET) 
		{
			closesocket(m_connectionSocket);
		}

		//SetOutEvent("EVT_ON_IDLE");
		SetOutEvent(_ON_IDLE_);
	}

	void SocketControl::CreateTimer()
	{
		PARENT->ZeroReceiveBuffer();

		Logger::logtrc('C',"[SOCKET] CreateTimer()");

		timer_conn.Create();
		timer_conn.start();		
	}

	void SocketControl::ConnectDestroyTimer()
	{		
		Logger::logtrc('C',"[SOCKET] ConnectDestroyTimer()");

		timer_conn.stop();
		timer_conn.Destroy();
		Connect();
	}

	void SocketControl::DisconnectDestroyTimer()
	{		
		Logger::logtrc('C',"[SOCKET] DisconnectDestroyTimer()");

		timer_conn.stop();
		timer_conn.Destroy();
		Disconnect();
		//SetOutEvent("EVT_ON_IDLE");
		SetOutEvent(_ON_IDLE_);
	}

	void SocketControl::TimerEventCallback(int tid)
	{
		if ( tid == TID_RECONN )		
		{
			if ( m_event != ONSTOP)
			{
				m_event = ONRECONNTOUT;
				Logger::logtrc('C',"[SOCKET] EVENT ONRECONNTOUT");
			}
		}
				
	}

	void SocketControl::Reconnect()
	{
		Logger::logtrc('C',"[SOCKET] Reconnect()");

		Disconnect();
		Connect();
	}
	
};