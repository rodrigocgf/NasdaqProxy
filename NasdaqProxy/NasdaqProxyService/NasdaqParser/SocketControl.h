#ifndef _SOCKET_CONTROL_H_
#define _SOCKET_CONTROL_H_

#pragma once

#include "..\stdafx.h"
#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include "EventTimer.h"
#include "..\ServiceCore\Config.h"

namespace NasdaqProxy  
{
	using std::string;
	using boost::shared_ptr;	


	#define TID_RECONN		3
	
	namespace SOCKETCONTROL
	{
		const int REMAIN = -1;
		const int NO_EVENT = -1;		
	};

	class SocketControl
	{
	public:
		SocketControl(void * pParent  ,Config lConfig );
		~SocketControl(void);

		int						m_state;
		int						_currentState;
		int						m_event;
		
		int						m_intEventSocket;

		boost::mutex			m_mtxWaitSocket;
		boost::xtime			m_waitInterval;

		DWORD					mError;

		string					_host;
		int						_port;
		int						_reconnectionInterval;
		//DWORD					m_loopInterval;
		bool					b_loopWait;

		SOCKET					m_connectionSocket;
		SOCKADDR_IN				m_HostAddress;
		int						m_Port;
		struct sockaddr_in		m_SockAddr;

		void *				m_ParentPtr;
		Config				m_Config;
	                    
		enum STATES
		{
			IDLE=0,
			TRY_CONNECT,
			WAIT_TO_RECONNECT,
			CONNECTED,
			SIZEOF_STATES
		};

		enum EVENTS
		{
			ONSTART =0,
			ONCONNECT,
			ONERROR,			
			ONRECONNTOUT,	
			ONDISCONNECT,			
			ONSTOP,
			SIZEOF_EVENTS
		};
	
		typedef void (SocketControl::* MPFUNC)(void);	

		typedef struct STATE_MACHINE 
		{
			int i_Proximo_Estado;
			MPFUNC acao;
			
			STATE_MACHINE(){}
			STATE_MACHINE(char proximo_Estado, MPFUNC lAcao)
			{
				acao = lAcao;
				i_Proximo_Estado = proximo_Estado;
			}
			
		} st_STATE_MACHINE;

		st_STATE_MACHINE _stateMachineSocket[SIZEOF_STATES][SIZEOF_EVENTS];

		boost::shared_ptr<boost::thread> _socketThreadPtr;

		boost::mutex					m_IncommingQueueLock;
		std::deque<int>				m_IncomingMessageQueue;		
		
		event_timer<SocketControl>		timer_conn;		

		void			SetOutEvent(int iEvent);
		int				GetInEvent();
		int				Send(unsigned char * p_buffer, int BytesToSend);
		int				Receive(unsigned char *Buffer , int	BytesToRead);
		unsigned long	GetSockAvailableBytes();
		void			TimerEventCallback(int tid);
		void			Start();
		void			Stop();
	private :
		
		void			InitializeStateMachine();
		void			ConfigureStateMachine();
		void			MainThreadLoop();
		void			LogStateMachine();				
		void			MonitorEvents();
		int				WaitToReceive();		
		bool			DoWait();
        
		//
		// STATE MACHINE FUNCTIONS
		//
		void			nop();
		void			Connect();
		void			ConnectionOK();
		void			Disconnect();
		void			CreateTimer();
		void			DisconnectDestroyTimer();
		void			ConnectDestroyTimer();		
		void			Reconnect();		
	};

};

#endif