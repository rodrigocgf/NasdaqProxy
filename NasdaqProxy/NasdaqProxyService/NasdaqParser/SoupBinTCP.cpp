/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: SoupBinTCP.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a	
	Date	: june 9 , 2010
=========================================================

*/
#include "StdAfx.h"
#include "SoupBinTCP.h"
#include "..\ServiceCore\NasdaqService.h"
#include "Logger.h"


#define	PARENT ((CNasdaqService *)m_ParentPtr)


namespace NasdaqProxy 
{

	SoupBinTCP::SoupBinTCP( Config lConfig, void * parent) 	:	timer_send( this,&SoupBinTCP::TimerEventCallback, TID_HB_SEND) ,
																timer_recv( this, &SoupBinTCP::TimerEventCallback , TID_HB_RECV) ,
																timer_login( this,&SoupBinTCP::TimerEventCallback, TID_LOGIN),
																m_Config(lConfig)
	{
		m_SoupBinMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		m_ParentPtr = parent;
		
		//mstr_msgSeqNum = "1";
		b_loopWait = true;
		d_msgSeqNum = 1;
		mstr_sessionID.clear();
		
		InitializeStateMachine();
		ConfigureStateMachine();
		
		m_socketControlPtr.reset( new SocketControl(this,lConfig) );

		m_soupBinTCPThreadPtr.reset ( new boost::thread ( boost::bind (&SoupBinTCP::MainThreadLoop, this)));

		timer_send.set_interval(1000);		
		timer_recv.set_interval(3000);
		timer_login.set_interval(3000);		

		// Fill LOGIN REQUEST
		
		strncpy(m_LoginRequestMsg.alfa_UserName, m_Config.m_strUserName.c_str(), sizeof(m_LoginRequestMsg.alfa_UserName));
		strncpy(m_LoginRequestMsg.alfa_Password, m_Config.m_strPassword.c_str(), sizeof(m_LoginRequestMsg.alfa_Password));
		strncpy(m_LoginRequestMsg.alfa_RequestedSession, mstr_sessionID.c_str(), sizeof(m_LoginRequestMsg.alfa_RequestedSession));
		strncpy(m_LoginRequestMsg.num_RequestedSeqNum  , GetLastSequenceNumber().c_str(), sizeof(m_LoginRequestMsg.num_RequestedSeqNum));
	}

	SoupBinTCP::~SoupBinTCP(void)
	{	
		Logger::logtrc('C',"SoupBinTCP::~SoupBinTCP()");
	}

	void SoupBinTCP::Start()
	{
		m_intEventSoup = 0;
		
		Logger::logtrc('C',"SoupBinTCP::Start()");
		m_event = EVT_START;		
	}

	void SoupBinTCP::Stop()
	{			
		Logger::logtrc('C',"SoupBinTCP::Stop()");

		timer_send.Destroy();
		timer_recv.Destroy();
		timer_login.Destroy();
		m_event = EVT_STOP;
	}

	void SoupBinTCP::WaitStop(boost::condition & paramConditionStop)
	{		
		Logger::logtrc('C',"SoupBinTCP::WaitStop() Init");

		boost::mutex						mtxWait;
		boost::mutex::scoped_lock			waitLock(mtxWait);
		m_conditionStop.wait(waitLock);

		paramConditionStop.notify_all();		
		
		{
			//boost::mutex::scoped_lock	lmtxWaitLock(m_mutexSoupBinTCP);		
			m_intEventSoup = 1;
		}

		m_soupBinTCPThreadPtr->join();		
		
		Logger::logtrc('C',"SoupBinTCP::WaitStop() Finish");
	}

	void SoupBinTCP::RequestMessage(long sequenceNumber)
	{

	}	

	void SoupBinTCP::InitializeStateMachine()
    {	
		for (int i = 0; i < (int)SIZEOF_STATES; i++)
        {
            for (int j = 0; j < (int) SIZEOF_EVENTS; j++)
            {
				_stateMachineSoupBinTCP[i][j] = st_STATE_MACHINE(SOUPBINTCP::REMAIN, &SoupBinTCP::nop );
            }
        }
    }

	void SoupBinTCP::ConfigureStateMachine()
    {
		/*                    CURRENT STATE                   EVENT 	                            NEXT STATE                           ACTION      */
		_stateMachineSoupBinTCP[      IDLE           ][      EVT_START        ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,      &SoupBinTCP::StartSocketControl    );

		_stateMachineSoupBinTCP[ WAIT_TCP_CONNECTION ][     EVT_CONNECTED     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupBinTCP::SendLoginRequestInitial   );
		_stateMachineSoupBinTCP[ WAIT_TCP_CONNECTION ][     EVT_STOP          ] = st_STATE_MACHINE(  WAIT_SOCKET_IDLE  ,    &SoupBinTCP::StopSocketControl  );

		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][   EVT_LOGIN_ACCEPTED  ] = st_STATE_MACHINE(  LOGGED_ON         ,    &SoupBinTCP::ReadyToReceive            );
		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][   EVT_LOGIN_REJECTED  ] = st_STATE_MACHINE(  WAIT_TO_RECONNECT ,    &SoupBinTCP::WaitToReconnect           );
		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][     EVT_STOP          ] = st_STATE_MACHINE(   WAIT_SOCKET_IDLE ,    &SoupBinTCP::StopSocketControl  );
		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][    EVT_TOUT_LOGIN     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupBinTCP::SendLoginRequest          );
		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][   EVT_SOCKET_ERROR    ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,     &SoupBinTCP::DestroyTimerLogin  );

		_stateMachineSoupBinTCP[   WAIT_TO_RECONNECT ][    EVT_TOUT_LOGIN     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupBinTCP::SendLoginRequest          );
		_stateMachineSoupBinTCP[   WAIT_TO_RECONNECT ][     EVT_STOP          ] = st_STATE_MACHINE(   WAIT_SOCKET_IDLE ,    &SoupBinTCP::StopSocketControl  );

		_stateMachineSoupBinTCP[     LOGGED_ON       ][    EVT_HB_SENDTOUT    ] = st_STATE_MACHINE(     LOGGED_ON      ,       &SoupBinTCP::SendHeartbeat          );
		_stateMachineSoupBinTCP[     LOGGED_ON       ][    EVT_HB_RECVTOUT    ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,      &SoupBinTCP::Disconnect              );
		_stateMachineSoupBinTCP[     LOGGED_ON       ][ EVT_SOCKET_DATA_PACKET] = st_STATE_MACHINE(     LOGGED_ON      ,      &SoupBinTCP::ReceiveMessage          );
		_stateMachineSoupBinTCP[     LOGGED_ON       ][   EVT_END_OF_SESSION  ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupBinTCP::StopAndWait            );
		_stateMachineSoupBinTCP[     LOGGED_ON       ][    EVT_SOCKET_ERROR   ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupBinTCP::DestroyTimersHB        );
		_stateMachineSoupBinTCP[     LOGGED_ON       ][    EVT_STOP           ] = st_STATE_MACHINE( WAIT_SOCKET_IDLE   ,       &SoupBinTCP::StopSocketControl  );


		_stateMachineSoupBinTCP[   WAIT_SOCKET_IDLE  ][    EVT_SOCKET_IDLE    ] = st_STATE_MACHINE(        IDLE        ,       &SoupBinTCP::NotifyStop         );

		m_currentState = (int)IDLE;
		m_event = (int)SOUPBINTCP::NO_EVENT;
	}

	bool SoupBinTCP::DoWait()
	{
		bool bRet = false;		
			
		if ( b_loopWait)
		{
			boost::xtime_get(&m_waitInterval, boost::TIME_UTC);
			m_waitInterval.nsec += 1*1000; // 1 us
			
			boost::thread::sleep(m_waitInterval);
		}

		if ( m_intEventSoup == 1 )
			bRet = true;

		return bRet;
	}

	void SoupBinTCP::MainThreadLoop()
	{
		int actionEvent = SOCKETCONTROL::NO_EVENT;
		
		while ( !DoWait())
		{
			m_state = m_currentState;

			if (m_event != SOUPBINTCP::NO_EVENT)
			{
				if (_stateMachineSoupBinTCP[m_currentState][m_event].i_Proximo_Estado != SOUPBINTCP::REMAIN)
				{
					m_currentState = _stateMachineSoupBinTCP[m_currentState][ m_event].i_Proximo_Estado;

					DisplayState();
				}
				actionEvent = m_event;

				m_event = SOUPBINTCP::NO_EVENT;

				(this->*_stateMachineSoupBinTCP[m_state][actionEvent].acao)();
				
			}

			MonitorEvents();
		}
		
		Logger::logtrc('C',"SoupBinTCP::MainThreadLoop Finish");		
	}
	
	void SoupBinTCP::DisplayState()
	{
		switch ( m_currentState )
		{	
		case IDLE:
			Logger::logtrc('C',"[SOUPBINTCP] STATE IDLE");
			break;
		case WAIT_TCP_CONNECTION:
			Logger::logtrc('C',"[SOUPBINTCP] STATE WAIT_TCP_CONNECTION");
			break;
		case WAIT_LOGIN:
			Logger::logtrc('C',"[SOUPBINTCP] STATE WAIT_LOGIN");
			break;
		case WAIT_SOCKET_IDLE:
			Logger::logtrc('C',"[SOUPBINTCP] STATE WAIT_SOCKET_IDLE");
			break;
		case WAIT_TO_RECONNECT:
			Logger::logtrc('C',"[SOUPBINTCP] STATE WAIT_TO_RECONNECT");
			break;
		case LOGGED_ON:
			//Logger::logtrc('C',"[SOUPBINTCP] STATE LOGGED_ON");
			break;

		}
		
	}

	void SoupBinTCP::MonitorEvents()
	{		
		b_loopWait = true;

		if ( m_IncomingMessageQueue.size() > 0 )
		{
			int  iEvent = GetInEvent();
			
			if ( iEvent == _CONNECTED_ )
			//if ( !strEvent.compare("EVT_CONNECTED"))
			{
				m_event = EVT_CONNECTED;
				Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_CONNECTED");
			} 
			else if ( iEvent == _SOCKET_ERROR_ )
			//else if ( !strEvent.compare("EVT_SOCKET_ERROR"))
			{
				m_event = EVT_SOCKET_ERROR;
				Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_SOCKET_ERROR");
			}
			else if ( iEvent == _SOCKET_DATA_  )
			//else if ( !strEvent.compare("EVT_SOCKET_DATA"))
			{
				//m_loopInterval = 0;
				b_loopWait = false;
				ReceiveMessage();				
			} 
			else if ( iEvent == _ON_IDLE_ )
			//else if ( !strEvent.compare("EVT_ON_IDLE"))
			{
				m_event = EVT_SOCKET_IDLE;
				Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_ON_IDLE");
			} 			
		}


	}	
	

	int SoupBinTCP::GetLastEvent()
	{
		int iEvent = _NO_EVENT_;
		
		if ( !m_IncomingMessageQueue.empty() )
			iEvent = m_IncomingMessageQueue[0];
			//iEvent = m_IncomingMessageQueue.back();

		return iEvent;
	}

	/// <summary>
	///	PUSH FRONT
	/// </summary>
	void SoupBinTCP::SetOutEvent(int iEvent)
	{	
		boost::mutex::scoped_lock lockKeeper(m_socketControlPtr->m_IncommingQueueLock);

		m_socketControlPtr->m_IncomingMessageQueue.push_front(iEvent);
	}

	/// <summary>
	///	GET BACK
	/// </summary>
	int SoupBinTCP::GetInEvent()
	{
		int iEvent = _NO_EVENT_;

		boost::mutex::scoped_lock lockKeeper(m_IncommingQueueLock);

		iEvent = m_IncomingMessageQueue.back();
		m_IncomingMessageQueue.pop_back();

		return iEvent;
	}


	void SoupBinTCP::nop()
	{

	}

	void SoupBinTCP::StartSocketControl()
	{
		Logger::logtrc('C',"[SOUPBINTCP] StartSocketControl()");
		m_socketControlPtr->Start();		
	}

	void SoupBinTCP::StopSocketControl()
	{
		m_socketControlPtr->Stop();		
		Logger::logtrc('C',"[SOUPBINTCP] StopSocketControl()");
	}

	/// <summary>
	/// Send LOGIN REQUEST with SessionID blank and Requested Sequence Number equal 1.	
	/// </summary>
	void SoupBinTCP::SendLoginRequestInitial()
	{
		Logger::logtrc('C',"[SOUPBINTCP] SendLoginRequestInitial()");

		timer_login.Create();
		timer_login.start();

		m_SoupBinMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		
		if ( m_socketControlPtr->Send( (unsigned char *)(&m_LoginRequestMsg), sizeof(MSG_LOGIN_REQUEST) ) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logerr('C',"[SOUPBINTCP] EVENT EVT_SOCKET_ERROR");
		}
		
	}

	/// <summary>
	/// Send LOGIN REQUEST with current SessionID Requested Sequence with the last received sequence number.	
	/// </summary>
	void SoupBinTCP::SendLoginRequest()
	{
		timer_login.start();

		// Fill LOGIN REQUEST SEQUENCE NUMBER

		if ( m_socketControlPtr->Send( (unsigned char *)&m_LoginRequestMsg, sizeof(MSG_LOGIN_REQUEST) ) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logerr('C',"[SOUPBINTCP] EVENT EVT_SOCKET_ERROR");
		}
	}


	/// <summary>
	/// Saves the received SessionID and start a timer for Heartbeat.
	/// </summary>
	void SoupBinTCP::ReadyToReceive()
	{
		Logger::logtrc('C',"[SOUPBINTCP] ReadyToReceive() ");

		timer_login.stop();
		timer_login.Destroy();

		
		mstr_sessionID.assign(m_LoginAccepted.alfa_Session,sizeof(m_LoginAccepted.alfa_Session));		
		
		//mstr_msgSeqNum.assign(m_LoginAccepted.num_SeqNum , sizeof(m_LoginAccepted.num_SeqNum));		

		timer_send.Create();
		timer_send.start();	

		timer_recv.Create();
		timer_recv.start();		
	}

	void SoupBinTCP::ReceiveMessage()
	{
		unsigned long iAvailableBytes = m_socketControlPtr->GetSockAvailableBytes();

		if ( iAvailableBytes > 0 )
		{
			char * p_buffeRecv = (char *)malloc(iAvailableBytes);

			if ( p_buffeRecv != NULL )
			{

				if ( m_socketControlPtr->Receive((unsigned char *)p_buffeRecv,iAvailableBytes) == iAvailableBytes )
				{
					for ( int index = 0 ; index < iAvailableBytes ; index++ )
					{
						if (m_SoupBinMsgState == 0)
						{                    
							m_messageSize = (WORD)((int)p_buffeRecv[index] & 0xff00);                    
							m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
							m_receiveIndex++;
							m_SoupBinMsgState = 1;
						} 
						else if ( m_SoupBinMsgState == 1 )
						{
							m_messageSize += (WORD)((int)p_buffeRecv[index] & 0x00ff);
							m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
							m_receiveIndex++;
							if (m_messageSize == 0)
							{
								m_receiveIndex = 0;
								continue;
							}
							
							m_SoupBinMsgState = 2;                     
						}
						else if (m_SoupBinMsgState == 2)
						{
							m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
							m_receiveIndex++;
							if (m_receiveIndex == (m_messageSize + 2) )
							{
								ParseMessage();
								m_SoupBinMsgState = 0;
								m_receiveIndex = 0;
							}
						}		
					}
				}

				free(p_buffeRecv);
			} else {
				Logger::logerr('C',"[SOUPBINTCP] Error allocating memory at ReceiveMessage().");
				m_event = EVT_SOCKET_ERROR;
			}


			timer_recv.start();
		}

	}

	/// <summary>
	/// Parses the received data and sends ITCH message buffer to UMDFITCH2FIXConv .
	/// </summary>
	void SoupBinTCP::ParseMessage()
	{
		char chMessageType = m_receiveBuffer[2];

		if ( chMessageType == SOUPBINTCP::LOGIN_ACCEPTED )
		{
			memcpy((char *)&m_LoginAccepted, m_receiveBuffer,sizeof(MSG_LOGIN_ACCEPTED));
			m_event = EVT_LOGIN_ACCEPTED;
			Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_LOGIN_ACCEPTED ");
		} 
		else if ( chMessageType == SOUPBINTCP::LOGIN_REJECTED) 
		{
			m_event = EVT_LOGIN_REJECTED;
			Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_LOGIN_REJECTED ");
		}
		else if ( chMessageType == SOUPBINTCP::SEQUENCED_DATA_PACKET )
		{
			int msgSize = 0;
			msgSize =	m_receiveBuffer[0] << 8;
			msgSize +=	m_receiveBuffer[1];
			
			m_SequencedDataPacket.p_Message = (char *)malloc(msgSize - 1);
			memset(m_SequencedDataPacket.p_Message,' ',sizeof(m_SequencedDataPacket.p_Message));

			memcpy((char *)&m_SequencedDataPacket, m_receiveBuffer, 3);			
			memcpy((char *)m_SequencedDataPacket.p_Message, (char *)&m_receiveBuffer[3],msgSize - 1);
			
			PARENT->SendToTibco( (unsigned char *)m_SequencedDataPacket.p_Message, msgSize - 1);
			free(m_SequencedDataPacket.p_Message);
			
			IncrementSequenceNumber();
		}
	}

	void SoupBinTCP::IncrementSequenceNumber()
	{
		/*
		int sequence;
		char szSequence[15];
		
		sequence = atoi(mstr_msgSeqNum.c_str());
		sequence++;
		memset(szSequence,0x00,sizeof(szSequence));
		sprintf(szSequence,"%d",sequence);
		mstr_msgSeqNum = szSequence;
		*/
		d_msgSeqNum += 1;
		
	}

	void SoupBinTCP::WaitToReconnect()
	{		
		timer_login.start();		

		Logger::logtrc('C',"[SOUPBINTCP] WaitToReconnect()");
	}


	void SoupBinTCP::SendHeartbeat()
	{
		MSG_HEARTBEAT_CLIENT heartbeat;

		static int pos = 0;
		if ( pos == 0 )
		{
			Logger::logtrcr('C',"[SOUPBINTCP] |");
			pos = 1;
		} else if ( pos == 1 ) {
			Logger::logtrcr('C',"[SOUPBINTCP] /");
			pos = 2;
		} else if ( pos == 2 ) {
			Logger::logtrcr('C',"[SOUPBINTCP] -");
			pos = 3;
		} else if ( pos == 3 ) {
			Logger::logtrcr('C',"[SOUPBINTCP] \\");
			pos = 0;
		} 
		//Logger::logtrc('C',"[SOUPBINTCP] SendHeartbeat()");
	
		if ( m_socketControlPtr->Send( (unsigned char *)&heartbeat, sizeof(MSG_HEARTBEAT_CLIENT)) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logerr('C',"[SOUPBINTCP] EVENT EVT_SOCKET_ERROR");
		}
		else
			timer_send.start();		

	}

	/// <summary>
	/// Sends EVT_STOP to Event Bus
	/// </summary>
	void SoupBinTCP::StopAndWait()
	{
		//SetOutEvent("EVT_STOP");
		SetOutEvent(_STOP_);

		timer_recv.Destroy();
		timer_send.Destroy();

		Logger::logtrc('C',"[SOUPBINTCP] StopAndWait()");
	}

	void SoupBinTCP::TimerEventCallback(int tid)
	{
		if ( m_event != EVT_STOP)
		{
			switch ( tid )
			{
			case TID_HB_SEND:
				m_event = EVT_HB_SENDTOUT;
				//Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_HB_SENDTOUT");
				break;
			case TID_HB_RECV:
				m_event = EVT_HB_RECVTOUT;
				Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_HB_RECVTOUT");
				break;
			case TID_LOGIN:
				Logger::logtrc('C',"[SOUPBINTCP] EVENT EVT_TOUT_LOGIN");
				m_event = EVT_TOUT_LOGIN;
				break;
			}
		}
	}

	void SoupBinTCP::Disconnect()
	{
		timer_recv.Destroy();
		timer_send.Destroy();

		SetOutEvent(_DISCONNECT_);		
		//SetOutEvent("EVT_DISCONNECT");		

		Logger::logtrc('C',"[SOUPBINTCP] Disconnect()");
	}

	void SoupBinTCP::DestroyTimersHB()
	{
		timer_recv.Destroy();
		timer_send.Destroy();
	}

	void SoupBinTCP::DestroyTimerLogin()
	{
		timer_login.stop();
		timer_login.Destroy();
	}

	void SoupBinTCP::NotifyStop()
	{		
		Logger::logtrc('C',"SoupBinTCP::NotifyStop()");

		m_conditionStop.notify_all();
	}

	string SoupBinTCP::GetLastSequenceNumber()
	{			
		//std::ostringstream oss;
		//oss << d_msgSeqNum;
		//std::string value = oss.str();
		//return value;

		char szValue[20];
		memset(szValue,0x00,sizeof(szValue));
		sprintf(szValue,"%.0f",d_msgSeqNum);
		return szValue;

		//return mstr_msgSeqNum;
	}

	void SoupBinTCP::ZeroReceiveBuffer()
	{
		m_receiveIndex = 0;
		memset( m_receiveBuffer, 0x00 , sizeof(m_receiveBuffer) );
	}

};
