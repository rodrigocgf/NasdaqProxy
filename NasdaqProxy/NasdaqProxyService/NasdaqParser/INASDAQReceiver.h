#pragma once

#pragma warning (disable : 4146 4267 4996)
#include <iostream>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
 
using namespace std;

class INASDAQReceiver
{	
public:
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void WaitStop(boost::condition & conditionStop) = 0;
	virtual void RequestMessage(long sequenceNumber) = 0;
	virtual void ParseMessage() = 0;
	virtual string GetLastSequenceNumber() = 0;
};

