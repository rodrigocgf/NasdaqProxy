#pragma once


#include "INASDAQReceiver.h"
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include "..\ServiceCore\Config.h"

namespace NasdaqProxy  
{
	using boost::shared_ptr;

	class ReceiverFactory 
	{
	private:
		ReceiverFactory(void) {}
		~ReceiverFactory(void) {}
		
	public:		
		static shared_ptr<INASDAQReceiver> CreateReceiver(Config lConfig, void * parent);
		
	};
};