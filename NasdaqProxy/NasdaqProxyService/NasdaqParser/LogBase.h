#ifndef _LOGBASE_H_
#define _LOGBASE_H_

#if _MSC_VER > 1000
#pragma once
#endif 

#include <iostream>
#include <time.h>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

namespace NasdaqProxy  
{	
	using namespace std;
	using namespace boost;
	
	class LogBase
	{
		friend class Logger;		

		boost::mutex	m_mutexLogger;
		
		bool			m_bLogData;
		bool			m_bLogControl;
		bool			m_bLogErrors;
		bool			m_bLogTrace;

		char			m_dir [1024];
		char			m_file [1024];
		FILE *			m_fd;

		void			OpenFile();
		void			CloseFile();
		void			dumplogtext(char* Title, unsigned char* Buf, int Size);
		void			gravalog ( char  * buffer, char tipo , bool newline = true);
		void			hexdump ( char * title, unsigned char * buff, int len);		
		void			printlin (FILE *f, unsigned char * buff, int offs, int len, int i_tambuff);
		
	};
}

#endif