#ifndef _EVENT_TIMER_H_
#define _EVENT_TIMER_H_

#include <iostream>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/lexical_cast.hpp>

#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>



namespace NasdaqProxy  
{
	using namespace boost;
	using namespace std;

	template<typename T>
	class event_timer
	{
		typedef void (T::* Callback)(int Param);
	public:				
		
		event_timer(	T * pParent, Callback pCallback  , const std::size_t& tid )	:	m_tid(tid),			
																						m_tick_count(0),
																						m_total_diff(0),
																						m_pParent(pParent),
																						m_pCallback(pCallback)
		{
			m_msInterval = 5;
			m_active = true;
			
			m_intEventTimer = 0;
		}

		
		~event_timer() 
		{ 
			Destroy(); 			
		}

		void Create()
		{
			m_intEventTimer = 0;
			m_threadPtr.reset ( new boost::thread ( boost::bind (&event_timer::MainThreadLoop, this)));
		}

		void Destroy()
		{
			m_active = false;			
			m_intEventTimer = 1;			
			
			boost::shared_ptr<boost::thread> nullPtr;
			if ( m_threadPtr != nullPtr)
				m_threadPtr->join();
			
		}

		void start()
		{
			m_active = true;
			m_before = boost::posix_time::microsec_clock::universal_time();			
		}

		void stop()
		{
			m_active = false;
			m_after = boost::posix_time::microsec_clock::universal_time();
			boost::posix_time::time_duration duration = m_after - m_before;			
		}

		void set_interval(const std::size_t& milliseconds)
		{
			m_msInterval = milliseconds;
		}

	private:

		boost::shared_ptr<boost::thread>	m_threadPtr;
		
		int									m_intEventTimer;
		boost::condition_variable			m_conditionTimer;
		boost::mutex						m_mtxTimerWait;
		bool								m_active;
		boost::xtime						m_waitInterval;

		bool DoWait()
		{
			bool bRet = false;
				
			boost::xtime_get(&m_waitInterval, boost::TIME_UTC);
			m_waitInterval.nsec += 3*1000; 
			
			boost::thread::sleep(m_waitInterval);				

			if ( m_intEventTimer == 1 )
				bRet = true;

			return bRet;
		}
		
		void MainThreadLoop()
		{	
			while ( !DoWait())
			{
				if ( m_active )
				{
					m_after = boost::posix_time::microsec_clock::universal_time();
					boost::posix_time::time_duration duration = m_after - m_before;

					if ( (double)m_msInterval <= (double)duration.total_milliseconds() )
					{
						OnTimer();
						m_active = false;
					}
				}
			}
		}
		
		void OnTimer()
		{
			(m_pParent->*m_pCallback)((int)m_tid);
		}
		
		Callback					m_pCallback;
		T							* m_pParent;		
		std::size_t					m_tid;
		std::size_t					m_msInterval;
		std::size_t					m_tick_count;		
		boost::posix_time::ptime	m_before;
		boost::posix_time::ptime	m_after;
		std::size_t					m_total_diff;
	};
}
#endif