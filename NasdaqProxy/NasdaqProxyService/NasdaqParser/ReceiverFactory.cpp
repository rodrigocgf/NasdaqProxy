#include "StdAfx.h"
#include "ReceiverFactory.h"
#include "SoupBinTCP.h"
#include "SoupTCPZlib.h"


namespace NasdaqProxy 
{
	using namespace boost::algorithm;

	shared_ptr<INASDAQReceiver> ReceiverFactory::CreateReceiver( Config lConfig, void * parent) 
	{		
		string configReceptionMode = to_upper_copy(lConfig.m_ReceptionMode);
		
		shared_ptr<INASDAQReceiver> p_receiver;
		if ( !configReceptionMode.compare("SOUPBINTCP") )
		{
			p_receiver =  (shared_ptr<INASDAQReceiver>) new SoupBinTCP( lConfig , parent);
		} 
		else if ( !configReceptionMode.compare("SOUPTCPZLIB" ) ) 
		{
			p_receiver =  (shared_ptr<INASDAQReceiver>) new SoupTCPZlib( lConfig , parent);
		}

		return p_receiver;

	}
}