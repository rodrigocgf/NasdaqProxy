/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: LogBase.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a		
	Date	: june 9 , 2010
=========================================================

*/
#include "Stdafx.h"
#include "Logger.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>


#define LOG_LEVEL_DEFAULT 3

namespace NasdaqProxy  
{
	void LogBase::OpenFile()
	{
		boost::mutex::scoped_lock	lmtxWaitLock(m_mutexLogger);

		
		char path[MAX_PATH];
		char ShortFilename[1024];
		char *ApChar;
		SYSTEMTIME st;

		GetLocalTime (&st);

		// Retira qualquer extensao que existir no nome do arquivo
		memcpy (ShortFilename,m_file,strlen(m_file));
		ShortFilename [strlen(m_file)]=0;
		if ( (ApChar=strchr(ShortFilename,'.')) != NULL )
		{
			*ApChar='\0';
		}     

		// Forca a extensao ".TXT" e tambem a gravacao de 
		// ano, mes e dia no nome do arquivo de log
		sprintf ( path, "%s\\%s%04d%02d%02d.log", m_dir, ShortFilename, st.wYear, st.wMonth , st.wDay );
	
		
		if ( ( m_fd = fopen ( path, "a+" ) ) == NULL )
		{				
			return;
		}
	}

	void LogBase::CloseFile()
	{
		fclose(m_fd);
	}

	void LogBase::gravalog ( char  * buffer , char tipo , bool newline )
	{			
		boost::mutex::scoped_lock	lmtxWaitLock(m_mutexLogger);
		
		SYSTEMTIME st;
		GetLocalTime (&st);		

		if ( newline )
		{
			fprintf(	m_fd, "[%04d%02d%02d %02d:%02d:%02d] %s \r\n",
				st.wYear, st.wMonth, st.wDay, st.wHour , 
				st.wMinute, st.wSecond, buffer );
		} else {		
			/*fpos_t position;
			for ( int i = 0 ; i < 10 ; i++ )
			{
				fgetpos(fd,&position);
				position = position - 2;
				fputc(' ',fd);
				position = position - 2;
				fsetpos(fd,&position);
				fflush(fd);
			}*/
			
			fprintf(	m_fd, "[%04d%02d%02d %02d:%02d:%02d] %s\r\n",
				st.wYear, st.wMonth, st.wDay, st.wHour , 
				st.wMinute, st.wSecond, buffer );
			
		}

		fflush(m_fd);

		/*
		fprintf(fd, "[%04d%02d%02d %02d:%02d:%02d.%03d](%c)[%d] %s \r\n",	
		st.wYear, st.wMonth, st.wDay, st.wHour , 
		st.wMinute, st.wSecond, st.wMilliseconds, tipo, 
		GetCurrentThreadId(), buffer );
		*/

		/*	
		fprintf(fd, "[%02d/%02d/%04d %02d:%02d:%02d.%03d] (%c)[%d] %s \r\n",
		st.wDay, st.wMonth, st.wYear, st.wHour , 
		st.wMinute, st.wSecond, st.wMilliseconds, tipo, 
		GetCurrentThreadId(), buffer );
		*/   

		/*
		fprintf(fd, "[%02d/%02d/%04d %02d:%02d:%02d.%03d] (%c)[%d] ",
		st.wDay, st.wMonth, st.wYear, st.wHour , 
		st.wMinute, st.wSecond, st.wMilliseconds, tipo ,
		GetCurrentThreadId() );
		//vfprintf(fd, buffer, ArgPtr);
		fprintf(fd,"%s", buffer);
		fprintf(fd,"\r\n");
		*/

		//fclose(fd);						
	}



	void LogBase::hexdump (char * title, unsigned char * buff, int len)
	{
		static boost::mutex::scoped_lock	lmtxWaitLock(m_mutexLogger);	


		time_t now;
		struct tm* dt;

		time(&now);
		dt = localtime(&now);

		fprintf(m_fd, "[%04d%02d%02d %02d:%02d:%02d][%d] %s \r\n",
			dt->tm_year+1900, dt->tm_mon+1, dt->tm_mday, dt->tm_hour, 
			dt->tm_min, dt->tm_sec, GetCurrentThreadId(), title );


		char oldbuff [16];			// Para remover linhas repetidas
		int  nlin  = len >> 4;		// len / 16
		int  resto = len & 0x0F;	// len % 16 
		int  first = 1;				// Flag: indica a primeira linha.
		int  i;

		// Tratamento de linhas completas
		for (i = 0; i < nlin; i++)
		{
			if (i != 0 && memcmp (oldbuff, &buff[i << 4], 16) == 0)
			{
				if (first)
				{
					// Imprimiremos apenas na 1a. linha de repeticao.
					fprintf (m_fd, "*\n");
					first = 0;
				}
			}
			else
			{
				first = 1;
				printlin (m_fd, buff, i << 4, 16,len);
			}
			memcpy (oldbuff, &buff[i << 4], 16);
		}
		// Tratamento do resto
		i = nlin;
		if (resto)
			printlin (m_fd, buff, i << 4, resto,len);
		fflush (m_fd);
		
	}

	void LogBase::printlin (FILE *f, unsigned char * buff, int offs, int len, int i_tambuff)		
	{
		int i,i_aux;
		BYTE b_val;	

		// Imprimindo o offset
		fprintf (f, "%04x\t", offs);
		// Imprimindo os caracteres em hexa
		for ( i = 0 ; i < 16 ; i++ ) {
			i_aux = offs + i;

			if ( i_aux < i_tambuff ) {
				b_val = (0xff & buff[offs+i]);
				// "0xfF &" remove o sinal 
				if (i < len)
					fprintf (f, "%02x ", b_val );
				else
					fprintf (f, "   ");
			} else {
				fprintf (f, "   ");
			}

			if (i == 7)     // espaco adicional entre colunas
				fprintf (f, " ");
		}

		// Imprimindo . para caracteres nao-imprimiveis
		fprintf (f, "  ");
		for (i = 0; i < len; i++)
			if (isprint (buff [offs+i]))
				putc (buff [offs+i], f);
			else
				putc ('.', f);
		putc ('\n', f);
	
	}	

	void LogBase::dumplogtext(char* Title, unsigned char* Buf, int Size)
	{
		static boost::mutex::scoped_lock	lmtxWaitLock(m_mutexLogger);	

		int        i;
		char       Marca  [64];		
		char*      pApt;
		
		SYSTEMTIME st;
		GetLocalTime(&st);

		
		sprintf(Marca, "%05d B %02d:%02d:%02d.%03d", GetCurrentThreadId(), st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		fprintf(m_fd, "%s %s\n", Marca, Title);
		for (i = 0; i < Size; i += 64)
			fprintf(m_fd, "%s   |%-64.64s|\n", Marca, (char*)&Buf[i]);

		fflush (m_fd);
		
	}

}
