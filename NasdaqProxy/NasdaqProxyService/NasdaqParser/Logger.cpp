/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: Logger.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a	
	Date	: june 9 , 2010
=========================================================

*/
#include "Stdafx.h"
#include "Logger.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h>


#define LOG_LEVEL_DEFAULT 3

namespace NasdaqProxy  
{
	
	Logger::Logger(char chType)
	{
		m_Config.LoadConfiguration();

		memset(m_dir, 0x00, sizeof(m_dir));
		memset(m_file, 0x00, sizeof(m_file));

		
		m_bLogData = m_Config.m_bLogData;
		m_bLogControl = m_Config.m_bLogControl;		
		m_bLogTrace = m_Config.m_bLogTrace;
		m_bLogErrors = m_Config.m_bLogErrors;
		

		if ( chType == 'D' )
		{			
			strcpy(m_dir, m_Config.m_strDataLogPath.c_str());			
			strcpy(m_file, m_Config.m_strDataLogName.c_str());

			OpenFile();
		} 
		else if ( chType == 'C' )
		{
			strcpy(m_dir, m_Config.m_strControlLogPath.c_str());			
			strcpy(m_file, m_Config.m_strControlLogName.c_str());

			OpenFile();
		}
	}	

	Logger::~Logger()
	{
		CloseFile();
	}

	bool Logger::AllowLogTrc(char chType)
	{		

		if ( (GetInstance(chType).m_bLogData == true) && (chType == 'D') )
		{			
			return true;
		}

		if ( (GetInstance(chType).m_bLogControl == true) && (chType == 'C') ) 
		{
			if ( GetInstance(chType).m_bLogTrace == false )
				return false;

			return true;
		} 

		return false;
	}

	bool Logger::AllowLogErr(char chType)
	{		

		if ( (GetInstance(chType).m_bLogData == true) && (chType == 'D') )
		{			
			return true;
		}

		if ( (GetInstance(chType).m_bLogControl == true) && (chType == 'C') ) 
		{
			if ( GetInstance(chType).m_bLogErrors == false )
				return false;

			return true;
		} 

		return false;
	}

	void Logger::logsys( char chType , char  * msg, ...)
	{
		char buffer[2048];

		va_list ArgPtr;
		va_start(ArgPtr, msg);
		vsprintf(buffer, msg, ArgPtr);
		va_end(ArgPtr);

		GetInstance(chType).gravalog ( buffer, 'S' );
	}
	
	
	void Logger::logerr(char chType , char  * msg, ...)
	{
		if ( !AllowLogErr(chType))
			return;

		char buffer[2048];

		va_list ArgPtr;
		va_start(ArgPtr, msg);
		vsprintf(buffer, msg, ArgPtr);
		va_end(ArgPtr);

		GetInstance(chType).gravalog ( buffer, 'E' );
	}
	
	void Logger::logtrc(char chType , char *msg, ...)
	{
		if ( !AllowLogTrc(chType))
			return;

		char buffer[2048];
		
		va_list ArgPtr;
		va_start(ArgPtr, msg);
		vsprintf(buffer, msg, ArgPtr);
		va_end(ArgPtr);


		GetInstance(chType).gravalog ( buffer, 'T' );
	}	

	void Logger::logtrc(char chType , long log_level, char  * msg, ...)
	{
		if ( !AllowLogTrc(chType))
			return;

		char buffer[2048];

		va_list ArgPtr;
		va_start(ArgPtr, msg);
		vsprintf(buffer, msg, ArgPtr);
		va_end(ArgPtr);

		GetInstance(chType).gravalog ( buffer, 'T' );
	}
	
	void Logger::logtrcr(char chType , char *msg, ... )
	{
		if ( !AllowLogTrc(chType))
			return;

		char buffer[2048];
		
		va_list ArgPtr;
		va_start(ArgPtr, msg);
		vsprintf(buffer, msg, ArgPtr);
		va_end(ArgPtr);


		GetInstance(chType).gravalog ( buffer, 'T' ,false);
	}
	
	
	void Logger::logdump(char chType , char * title, unsigned char * buf, int siz)
	{	
		if ( !AllowLogTrc(chType))
			return;
		
		GetInstance(chType).hexdump(title, buf, siz);		
	}

	void Logger::logtext( char chType , char* Title, unsigned char* Buf, int Size)
	{
		if ( !AllowLogTrc(chType))
			return;

		GetInstance(chType).dumplogtext(Title, Buf, Size);
	}
	
	
}