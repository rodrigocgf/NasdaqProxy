
#if !defined(AFX_LOGAPIFUNC_H__DCD40081_F9F1_11D3_93A8_00A0C93D342D__INCLUDED_)
#define AFX_LOGAPIFUNC_H__DCD40081_F9F1_11D3_93A8_00A0C93D342D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif 

#include "LogBase.h"
#include <time.h>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>

#include "..\ServiceCore\Config.h"

namespace NasdaqProxy  
{	
	using namespace std;
	using namespace boost;
	using namespace NasdaqProxy;

	class Logger : public LogBase
	{
	private:
		Logger(char chType);		
		~Logger();
		
	public:
		Config m_Config;

		static Logger &GetInstance(char type)
		{	
			if ( type == 'C' )
			{
				static Logger *instance = new Logger('C');			
				return *instance;
			} 
			else if ( type == 'D' )
			{
				static Logger *instance = new Logger('D');
				return *instance;
			}
			
		}
	
		static bool AllowLogTrc(char chType);
		static bool AllowLogErr(char chType);
	
		static void logsys ( char chType , char * msg, ...);
		static void logtrc ( char chType , char * msg, ... );
		static void logtrcr( char chType , char * msg, ... );
		static void logtrc ( char chType , long log_level, char * msg,...);
		static void logerr ( char chType , char * msg, ... );	
		static void logdump( char chType , char * title, unsigned char * buf, int siz);
		static void	logtext( char chType , char* Title, unsigned char* Buf, int Size);
	};
	
}

#endif 
