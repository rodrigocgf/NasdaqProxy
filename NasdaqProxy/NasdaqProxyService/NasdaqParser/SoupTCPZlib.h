#ifndef _SOUPTCPZLIB_H_
#define _SOUPTCPZLIB_H_

#pragma once
#include "..\stdafx.h"
#include "INASDAQReceiver.h"

#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>

#include "SocketControl.h"
#include "zlib.h"
#include "..\ServiceCore\Config.h"
#include "Constants.h"

namespace NasdaqProxy 
{
	using boost::shared_ptr;

	namespace SOUPTCPZLIB
	{
		const int REMAIN = -1;
		const int NO_EVENT = -1;
		const int THREAD_LOOP_INTERVAL = 1;
		const int MAX_BUFFER_SIZE = 100000;

		const char LOGIN_ACCEPTED = 'A';
		const char LOGIN_REQUEST = 'L';
		const char LOGIN_REJECTED = 'J';
		const char SEQUENCED_DATA_PACKET = 'S';
		const char HEARTBEAT_CLIENT = 'R';
		const char LOGOUT_REQUEST = 'O';
	};

	typedef struct SOUPTCP_LOGIN_REQUEST
	{
		SOUPTCP_LOGIN_REQUEST()
		{
			ch_PacketType = SOUPTCPZLIB::LOGIN_REQUEST;
			memset(alfa_UserName,0x00,sizeof(alfa_UserName));
			memset(alfa_Password,0x00,sizeof(alfa_Password));
			memset(alfa_RequestedSession,' ',sizeof(alfa_RequestedSession));
			memset(alfa_RequestedSeqNum,0x00,sizeof(alfa_RequestedSeqNum));
			chLineFeed = 0x0A;
		}

		char ch_PacketType;
		char alfa_UserName[6];
		char alfa_Password[10];
		char alfa_RequestedSession[10];
		char alfa_RequestedSeqNum[10];
		char chLineFeed;

	} MSG_SOUPTCP_LOGIN_REQUEST;

	typedef struct SOUPTCP_LOGIN_ACCEPTED
	{
		SOUPTCP_LOGIN_ACCEPTED()
		{
			ch_PacketType = SOUPTCPZLIB::LOGIN_ACCEPTED;
			memset(alfa_Session,0x00,sizeof(alfa_Session));
			memset(num_SeqNum,0x00,sizeof(num_SeqNum));
			chLineFeed = 0x0A;
		}

		char ch_PacketType;
		char alfa_Session[10];
		char num_SeqNum[20];
		char chLineFeed;
	} MSG_SOUPTCP_LOGIN_ACCEPTED;

	typedef struct SOUPTCP_LOGIN_REJECTED
	{
		SOUPTCP_LOGIN_REJECTED()
		{
			ch_PacketType = SOUPTCPZLIB::LOGIN_REJECTED;
			chLineFeed = 0x0A;
		}

		char ch_PacketType;
		char ch_RejectReasonCode;
		char chLineFeed;
	} MSG_SOUPTCP_LOGIN_REJECTED;

	typedef struct SOUPTCP_SEQUENCED_DATA_PACKET
	{
		char	ch_PacketType;
		char *	p_Message;			
	} MSG_SOUPTCP_SEQUENCED_DATA_PACKET;

	typedef struct SOUPTCP_HEARTBEAT_CLIENT
	{
		SOUPTCP_HEARTBEAT_CLIENT()
		{
			ch_PacketType = SOUPTCPZLIB::HEARTBEAT_CLIENT;
			chLineFeed = 0x0A;
		}		
		char    ch_PacketType;
		char chLineFeed;
	} MSG_SOUPTCP_HEARTBEAT_CLIENT;

	typedef struct SOUPTCP_LOGOUT_REQUEST
	{
		SOUPTCP_LOGOUT_REQUEST()
		{
			ch_PacketType = SOUPTCPZLIB::LOGOUT_REQUEST;
			chLineFeed = 0x0A;
		}

		char ch_PacketType;
		char chLineFeed;

	} MSG_SOUPTCP_LOGOUT_REQUEST;

	#define	TID_HB_SEND_SOUPTCP		1
	#define	TID_HB_RECV_SOUPTCP		2
	#define TID_LOGIN_SOUPTCP		3

	class SoupTCPZlib : public INASDAQReceiver
	{
	public:
		SoupTCPZlib(Config lConfig, void * parent);
		~SoupTCPZlib(void);

		void Start();
		void Stop();
		void WaitStop(boost::condition & conditionStop);
		void RequestMessage(long sequenceNumber);
		void ParseMessage();

		int					m_SoupTcpZlibMsgState;
		int					m_state;
		int					m_currentState;
		int					m_event;
		
		int							m_intEventSoup;
		boost::condition_variable	m_condition;
		boost::condition_variable	m_conditionStop;

		boost::mutex				m_mutexSoupBinTCP;
		boost::xtime				m_waitInterval;
		Config				m_Config;
		boost::mutex		m_IncommingQueueLock;
		std::deque<int>		m_IncomingMessageQueue;

		enum STATES
		{
			IDLE=0,
			WAIT_TCP_CONNECTION,
			WAIT_LOGIN,
			WAIT_SOCKET_IDLE,
			WAIT_TO_RECONNECT,
			LOGGED_ON,
			SIZEOF_STATES
		};

		enum EVENTS
		{			
			EVT_CONNECTED = 0,
			EVT_START,
			EVT_STOP,
			EVT_SOCKET_IDLE,
			EVT_TOUT_LOGIN,
			EVT_HB_SENDTOUT,
			EVT_HB_RECVTOUT,
			EVT_LOGIN_ACCEPTED,
			EVT_LOGIN_REJECTED,
			EVT_SOCKET_DATA_PACKET,
			EVT_SOCKET_ERROR,
			EVT_END_OF_SESSION,
			SIZEOF_EVENTS
		};

		typedef void (SoupTCPZlib::* MPFUNC)(void);	

		typedef struct STATE_MACHINE 
		{
			int i_Proximo_Estado;
			MPFUNC acao;
			
			STATE_MACHINE(){}
			STATE_MACHINE(char proximo_Estado, MPFUNC lAcao)
			{
				acao = lAcao;
				i_Proximo_Estado = proximo_Estado;
			}
			
		} st_STATE_MACHINE;

		st_STATE_MACHINE m_stateMachineSoupTCPZlib[SIZEOF_STATES][SIZEOF_EVENTS];

		boost::shared_ptr<boost::thread> m_soupBinTCPThreadPtr;

		boost::shared_ptr<SocketControl> m_socketControlPtr;
		z_stream m_strm;		
		
		void	SetOutEvent(int iEvent);
		int	GetInEvent();
		int	GetLastEvent();

		//
		// SoupTCPZlib messages structs
		//
		
		MSG_SOUPTCP_LOGIN_REQUEST	m_LoginRequestMsg;
		
		MSG_SOUPTCP_LOGIN_ACCEPTED m_LoginAccepted;		

		MSG_SOUPTCP_SEQUENCED_DATA_PACKET m_SequencedDataPacket;

		string GetLastSequenceNumber();

		void ZeroReceiveBuffer();

	private:
		
		void	TimerEventCallback(int tid);
		void	InitializeStateMachine();
		void	ConfigureStateMachine();
		void	MainThreadLoop();
		void	MonitorEvents();
		bool	DoWait();
		void	IncrementSequenceNumber();

		int		ZlibDecompress(unsigned char * p_in , unsigned char * p_out , int in_size, int out_size);

		char 		m_receiveBuffer[SOUPTCPZLIB::MAX_BUFFER_SIZE];
        int			m_receiveIndex;
		int			m_messageSize;		

		//
		// STATE MACHINE FUNCTIONS
		//
		void nop();
		void SendLoginRequest();
		void SendLoginRequestInitial();
		void ReadyToReceive();
		void ReceiveMessage();
		void WaitToReconnect();
		void SendHeartbeat();
		void StopAndWait();
		void Disconnect();
		void DestroyTimersHB();
		void StartSocketControl();
		void StopSocketControl();
		void NotifyStop();
		void DestroyTimerLogin();

		//DWORD										m_loopInterval;
		bool										b_loopWait;
		string										mstr_sessionID;
		string										mstr_msgSeqNum;	
		void *										m_ParentPtr;

		event_timer<SoupTCPZlib>						timer_send;
		event_timer<SoupTCPZlib>						timer_recv;
		event_timer<SoupTCPZlib>						timer_login;

		void DisplayState();		
	};


}

#endif