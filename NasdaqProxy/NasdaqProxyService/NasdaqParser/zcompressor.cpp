# ifndef _UNUSED_
#   ifdef __GNUC__
#     define _UNUSED_ __attribute__ ((__unused__))
#   else
#     define _UNUSED_
#   endif
# endif

#include "zlib.h"

static const char* Compressor_cpp_Rev _UNUSED_="@(#) $Name: HEAD $ $Header: /home/dev/cvsroot/DTM/LIB/libzlib/zcompressor.cpp,v 1.5 2008/06/27 09:01:37 fred Exp $";

/**************************************************************************************************/
ZlibCompression::ZlibCompression() :
  defStream(0),
  infStream(0),
  zRc(Z_OK)
{
}

ZlibCompression::ZlibCompression(int level) :
  defStream(0),
  infStream(0),
  zRc(Z_OK)
{
  Init(level);
}

ZlibCompression::~ZlibCompression()
{
  Close();
}

void ZlibCompression::Close()
{
  if(infStream!=0)
  {
    inflateEnd(infStream);
    delete infStream;
    infStream=0;
  }
  if(defStream!=0)
  {
    deflateEnd(defStream);
    delete defStream;
    defStream=0;
  }
}

bool ZlibCompression::Init(int level)
{
  Close();

  infStream=new z_stream;
  defStream=new z_stream;
  if(infStream!=0 && defStream!=0)
  {
    infStream->zalloc=Z_NULL;
    infStream->zfree=Z_NULL;
    infStream->opaque=Z_NULL;
    defStream->zalloc=Z_NULL;
    defStream->zfree=Z_NULL;
    defStream->opaque=Z_NULL;
    infStream->next_in=Z_NULL;
    infStream->avail_in=0;

    if(inflateInit(infStream)==Z_OK && deflateInit(defStream,level)==Z_OK)
    {
      return(true);
    }
  }

  Close();
  return(false);
}

int ZlibCompression::Deflate(void* Dest,const void* Source,int DestSize,int SourceSize)
{
  defStream->next_in=(Bytef*)(Source);
  defStream->avail_in=SourceSize;
  defStream->next_out=(Bytef*)(Dest);
  defStream->avail_out=DestSize;
  zRc=deflate(defStream,Z_PARTIAL_FLUSH);
  return(zRc==Z_OK ? DestSize-(int)defStream->avail_out : -1);
}

int ZlibCompression::Flush(void* Dest,int DestSize)
{
  if(defStream->avail_in==0)
  {
    defStream->next_in=Z_NULL;
    defStream->avail_in=0;
    defStream->next_out=(Bytef*)(Dest);
    defStream->avail_out=DestSize;
    zRc=deflate(defStream,Z_FULL_FLUSH);
    if(zRc==Z_OK && defStream->avail_out!=0)
    {
      return(DestSize-defStream->avail_out);
    }
  }

  return(-1);
}

bool ZlibCompression::Sync(void const* Source,int SourceSize)
{
  infStream->next_in=(Bytef*)(Source);
  infStream->avail_in=SourceSize;
  infStream->next_out=Z_NULL;
  infStream->avail_out=0;
  zRc=inflateSync(infStream);
  return(zRc==Z_OK && infStream->avail_in==0);
}

int ZlibCompression::Inflate(void* Dest,const void* Source,int DestSize,int SourceSize)
{
  infStream->next_in=(Bytef*)(Source);
  infStream->avail_in=SourceSize;
  infStream->next_out=(Bytef*)(Dest);
  infStream->avail_out=DestSize;
  zRc=inflate(infStream,Z_PARTIAL_FLUSH);
  return(zRc==Z_OK ? DestSize-(int)infStream->avail_out : -1);
}

/**************************************************************************************************/

ZlibCompress::ZlibCompress() :
  m_Valid(false)
{
  Init(Z_DEFAULT_COMPRESSION);
}

ZlibCompress::ZlibCompress(int p_Level) :
  m_Valid(true)
{
  m_Stream.zalloc=Z_NULL;
  m_Stream.zfree=Z_NULL;
  m_Stream.opaque=Z_NULL;
  if(deflateInit(&m_Stream,p_Level)!=Z_OK)
  {
    m_Valid=false;
  }
}

ZlibCompress::~ZlibCompress()
{
  if(m_Valid==true)
  {
    deflateEnd(&m_Stream);
  }
}

bool ZlibCompress::Init(int p_Level)
{
  if(m_Valid==true)
  {
    deflateEnd(&m_Stream);
  }

  m_Stream.zalloc=Z_NULL;
  m_Stream.zfree=Z_NULL;
  m_Stream.opaque=Z_NULL;
  m_Valid=deflateInit(&m_Stream,p_Level)==Z_OK;

  return(m_Valid);
}

int ZlibCompress::Compress(void* p_Dest,
                           const void* p_Source,
                           int p_DestSize,
                           int p_SourceSize,
                           int p_FlushMode)
{
  try
  {
    if(m_Valid==true)
    {
      m_Stream.next_in=(Bytef*)p_Source;
      m_Stream.avail_in=p_SourceSize;
      m_Stream.next_out=(Bytef*)p_Dest;
      m_Stream.avail_out=p_DestSize;
      if(deflate(&m_Stream,p_FlushMode)==Z_OK && m_Stream.avail_out!=0)
      {
        return(p_DestSize-m_Stream.avail_out);
      }
    }
  }
  catch(...)
  {
  }

  return(-1);
}

int ZlibCompress::Flush(void* Dest,int DestSize)
{
  try
  {
    if(m_Valid==true)
    {
      m_Stream.next_in=Z_NULL;
      m_Stream.avail_in=0;
      m_Stream.next_out=(Bytef*)(Dest);
      m_Stream.avail_out=DestSize;
      if(deflate(&m_Stream,Z_FULL_FLUSH)==Z_OK)
      {
        return(DestSize-m_Stream.avail_out);
      }
    }
  }
  catch(...)
  {
  }

  return(-1);
}

bool ZlibCompress::Reset()
{
  return(m_Valid==true && deflateReset(&m_Stream)==Z_OK);
}

/**************************************************************************************************/

ZlibUncompress::ZlibUncompress() :
  m_Valid(true)
{
  m_Stream.zalloc=Z_NULL;
  m_Stream.zfree=Z_NULL;
  m_Stream.opaque=Z_NULL;
  m_Stream.next_in=Z_NULL;
  m_Stream.avail_in=0;

  if(inflateInit(&m_Stream)!=Z_OK)
  {
    m_Valid=false;
  }
}

ZlibUncompress::~ZlibUncompress()
{
  if(m_Valid==true)
  {
    inflateEnd(&m_Stream);
  }
}

bool ZlibUncompress::Reset()
{
  return(m_Valid==true && inflateReset(&m_Stream)==Z_OK);
}

bool ZlibUncompress::Finish(const void* p_Source,int p_SourceSize)
{
  try
  {
    if(m_Valid==true)
    {
      m_Stream.next_in=(Bytef*)p_Source;
      m_Stream.avail_in=p_SourceSize;
      m_Stream.next_out=0;
      m_Stream.avail_out=0;
      return(inflate(&m_Stream,Z_FULL_FLUSH)==Z_OK);
    }
  }
  catch(...)
  {
  }

  return(false);
}

int ZlibUncompress::Uncompress(void* p_Dest,
                               const void* p_Source,
                               int p_DestSize,
                               int p_SourceSize,
                               int p_FlushMode)
{
  try
  {
    if(m_Valid==true)
    {
      m_Stream.next_in=(Bytef*)p_Source;
      m_Stream.avail_in=p_SourceSize;
      m_Stream.next_out=(Bytef*)p_Dest;
      m_Stream.avail_out=p_DestSize;
      if(inflate(&m_Stream,p_FlushMode)==Z_OK && m_Stream.avail_out!=0)
      {
        return(p_DestSize-m_Stream.avail_out);
      }
    }
  }
  catch(...)
  {
  }

  return(-1);
}
