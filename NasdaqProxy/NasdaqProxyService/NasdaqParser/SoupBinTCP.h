#ifndef _SOUPTCP_H_
#define _SOUPTCP_H_

#pragma once
#include "..\stdafx.h"
#include "INASDAQReceiver.h"

#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include "EventTimer.h"

#include "SocketControl.h"
#include "..\ServiceCore\Config.h"
#include "Constants.h"

namespace NasdaqProxy 
{
	using boost::shared_ptr;	

	namespace SOUPBINTCP
	{
		const int REMAIN = -1;
		const int NO_EVENT = -1;
		const int THREAD_LOOP_INTERVAL = 1;
		const int MAX_BUFFER_SIZE = 20000;

		const char LOGIN_ACCEPTED = 'A';
		const char LOGIN_REJECTED = 'J';
		const char LOGIN_REQUEST = 'L';
		const char SEQUENCED_DATA_PACKET = 'S';
		const char HEARTBEAT_CLIENT = 'R';
	};

	typedef struct LOGIN_REQUEST
	{
		LOGIN_REQUEST()
		{
			ch_PacketType = SOUPBINTCP::LOGIN_REQUEST;
			memset(alfa_UserName,' ',sizeof(alfa_UserName));
			memset(alfa_Password,' ',sizeof(alfa_Password));
			memset(alfa_RequestedSession,' ',sizeof(alfa_RequestedSession));
			memset(num_RequestedSeqNum,0x00,sizeof(num_RequestedSeqNum));
		}


		char int_PacketLength[2];
		char ch_PacketType;
		char alfa_UserName[6];
		char alfa_Password[10];
		char alfa_RequestedSession[10];
		char num_RequestedSeqNum[20];			

	} MSG_LOGIN_REQUEST;

	typedef struct LOGIN_ACCEPTED
	{
		LOGIN_ACCEPTED()
		{
			memset(alfa_Session,0x00,sizeof(alfa_Session));
			memset(num_SeqNum,0x00,sizeof(num_SeqNum));
		}
		char int_PacketLength[2];
		char ch_PacketType;
		char alfa_Session[10];
		char num_SeqNum[20];			
	} MSG_LOGIN_ACCEPTED;

	typedef struct SEQUENCED_DATA_PACKET
	{
		char	int_PacketLength[2];
		char	ch_PacketType;
		char *	p_Message;			
	} MSG_SEQUENCED_DATA_PACKET;

	typedef struct HEARTBEAT_CLIENT
	{
		HEARTBEAT_CLIENT()
		{
			ch_PacketType = SOUPBINTCP::HEARTBEAT_CLIENT;
		}
		char	int_PacketLength[2];
		char    ch_PacketType;
	} MSG_HEARTBEAT_CLIENT;

	#define	TID_HB_SEND		1
	#define	TID_HB_RECV		2
	#define TID_LOGIN		3

	class SoupBinTCP : public INASDAQReceiver
	{
	public:		

		SoupBinTCP( Config lConfig, void * parent);		
		~SoupBinTCP(void);

		void Start();
		void Stop();
		void WaitStop(boost::condition & conditionStop);
		void RequestMessage(long sequenceNumber);
		void ParseMessage();

		int					m_SoupBinMsgState;
		int					m_state;
		int					m_currentState;
		int					m_event;
		
		int							m_intEventSoup;
		boost::condition_variable	m_condition;
		boost::condition_variable	m_conditionStop;

		boost::mutex				m_mutexSoupBinTCP;
		boost::xtime				m_waitInterval;
		Config				m_Config;
		boost::mutex		m_IncommingQueueLock;
		std::deque<int>	m_IncomingMessageQueue;

		enum STATES
		{
			IDLE=0,
			WAIT_TCP_CONNECTION,
			WAIT_LOGIN,
			WAIT_SOCKET_IDLE,
			WAIT_TO_RECONNECT,
			LOGGED_ON,
			SIZEOF_STATES
		};

		enum EVENTS
		{			
			EVT_CONNECTED = 0,
			EVT_START,
			EVT_STOP,
			EVT_SOCKET_IDLE,
			EVT_TOUT_LOGIN,
			EVT_HB_SENDTOUT,
			EVT_HB_RECVTOUT,
			EVT_LOGIN_ACCEPTED,
			EVT_LOGIN_REJECTED,
			EVT_SOCKET_DATA_PACKET,
			EVT_SOCKET_ERROR,
			EVT_END_OF_SESSION,
			SIZEOF_EVENTS
		};

		typedef void (SoupBinTCP::* MPFUNC)(void);	

		typedef struct STATE_MACHINE 
		{
			int i_Proximo_Estado;
			MPFUNC acao;
			
			STATE_MACHINE(){}
			STATE_MACHINE(char proximo_Estado, MPFUNC lAcao)
			{
				acao = lAcao;
				i_Proximo_Estado = proximo_Estado;
			}
			
		} st_STATE_MACHINE;

		st_STATE_MACHINE _stateMachineSoupBinTCP[SIZEOF_STATES][SIZEOF_EVENTS];

		boost::shared_ptr<boost::thread> m_soupBinTCPThreadPtr;

		boost::shared_ptr<SocketControl> m_socketControlPtr;
		
		
		void	SetOutEvent(int iEvent);
		int		GetInEvent();		
		int		GetLastEvent();

		//
		// SoupBinTCP messages structs
		//
		
		MSG_LOGIN_REQUEST	m_LoginRequestMsg;

		
		MSG_LOGIN_ACCEPTED m_LoginAccepted;
		

		MSG_SEQUENCED_DATA_PACKET m_SequencedDataPacket;

		string GetLastSequenceNumber();

		void ZeroReceiveBuffer();

	private:
		
		void TimerEventCallback(int tid);
		void InitializeStateMachine();
		void ConfigureStateMachine();
		void MainThreadLoop();
		void MonitorEvents();
		bool DoWait();
		void IncrementSequenceNumber();

		char 		m_receiveBuffer[SOUPBINTCP::MAX_BUFFER_SIZE];
        int			m_receiveIndex;
		int			m_messageSize;		

		//
		// STATE MACHINE FUNCTIONS
		//
		void nop();
		void SendLoginRequest();
		void SendLoginRequestInitial();
		void ReadyToReceive();
		void ReceiveMessage();
		void WaitToReconnect();
		void SendHeartbeat();
		void StopAndWait();
		void Disconnect();
		void DestroyTimersHB();
		void StartSocketControl();
		void StopSocketControl();
		void NotifyStop();
		void DestroyTimerLogin();

		//DWORD										m_loopInterval;
		bool										b_loopWait;
		string										mstr_sessionID;
		string										mstr_msgSeqNum;	
		double										d_msgSeqNum;
		void *										m_ParentPtr;

		event_timer<SoupBinTCP>						timer_send;
		event_timer<SoupBinTCP>						timer_recv;
		event_timer<SoupBinTCP>						timer_login;

		void DisplayState();		
	};

};

#endif