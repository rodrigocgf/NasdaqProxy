/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: SoupBinTCP.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a	
	Date	: june 9 , 2010
=========================================================

*/
#include "StdAfx.h"
#include "SoupTCPZlib.h"
#include "..\ServiceCore\NasdaqService.h"
#include "Logger.h"

#define	PARENT ((CNasdaqService *)m_ParentPtr)

namespace NasdaqProxy 
{

	SoupTCPZlib::SoupTCPZlib(Config lConfig, void * parent) :	timer_send( this,&SoupTCPZlib::TimerEventCallback, TID_HB_SEND_SOUPTCP) ,
																timer_recv( this, &SoupTCPZlib::TimerEventCallback , TID_HB_RECV_SOUPTCP) ,
																timer_login( this,&SoupTCPZlib::TimerEventCallback, TID_LOGIN_SOUPTCP) ,
																m_Config(lConfig)
	{
		m_SoupTcpZlibMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		m_ParentPtr = parent;

		//m_loopInterval = 0;
		b_loopWait = true;
		mstr_msgSeqNum = "1";
		mstr_sessionID.clear();
		
		InitializeStateMachine();
		ConfigureStateMachine();
		
		m_socketControlPtr.reset( new SocketControl(this,lConfig) );

		m_soupBinTCPThreadPtr.reset ( new boost::thread ( boost::bind (&SoupTCPZlib::MainThreadLoop, this)));

		timer_send.set_interval(1000);		
		timer_recv.set_interval(5000);
		timer_login.set_interval(3000);

		// Fill LOGIN REQUEST
		
		strncpy(m_LoginRequestMsg.alfa_UserName, m_Config.m_strUserName.c_str(), sizeof(m_LoginRequestMsg.alfa_UserName));
		strncpy(m_LoginRequestMsg.alfa_Password, m_Config.m_strPassword.c_str(), sizeof(m_LoginRequestMsg.alfa_Password));
		strncpy(m_LoginRequestMsg.alfa_RequestedSession, mstr_sessionID.c_str(), sizeof(m_LoginRequestMsg.alfa_RequestedSession));
		strncpy(m_LoginRequestMsg.alfa_RequestedSeqNum  , mstr_msgSeqNum.c_str(), sizeof(m_LoginRequestMsg.alfa_RequestedSeqNum));

		/* allocate inflate state */
		m_strm.zalloc = Z_NULL;
		m_strm.zfree = Z_NULL;
		m_strm.opaque = Z_NULL;
		m_strm.avail_in = 0;
		m_strm.next_in = Z_NULL;
		int ret = inflateInit(&m_strm);
		if (ret != Z_OK)
			Logger::logerr('C',"SoupTCPZlib::SoupTCPZlib() ZLIB InflateInit error !");
	}

	SoupTCPZlib::~SoupTCPZlib(void)
	{
		Logger::logtrc('C',"SoupTCPZlib::~SoupTCPZlib()");
	}

	void SoupTCPZlib::Start()
	{
		m_intEventSoup = 0;
		
		Logger::logtrc('C',"SoupBinTCP::Start()");
		m_event = EVT_START;		
	}

	void SoupTCPZlib::Stop()
	{
		Logger::logtrc('C',"SoupBinTCP::Stop()");

		timer_send.Destroy();
		timer_recv.Destroy();
		timer_login.Destroy();
		m_event = EVT_STOP;
	}

	void SoupTCPZlib::WaitStop(boost::condition & paramConditionStop)
	{
		Logger::logtrc('C',"SoupBinTCP::WaitStop() Init");

		boost::mutex						mtxWait;
		boost::mutex::scoped_lock			waitLock(mtxWait);
		m_conditionStop.wait(waitLock);

		paramConditionStop.notify_all();			
		m_intEventSoup = 1;

		m_soupBinTCPThreadPtr->join();		
		
		Logger::logtrc('C',"SoupBinTCP::WaitStop() Finish");
	}

	void SoupTCPZlib::RequestMessage(long sequenceNumber)
	{

	}

	void SoupTCPZlib::InitializeStateMachine()
    {	
		for (int i = 0; i < (int)SIZEOF_STATES; i++)
        {
            for (int j = 0; j < (int) SIZEOF_EVENTS; j++)
            {
				m_stateMachineSoupTCPZlib[i][j] = st_STATE_MACHINE(SOUPTCPZLIB::REMAIN, &SoupTCPZlib::nop );
            }
        }
    }

	void SoupTCPZlib::ConfigureStateMachine()
    {
		/*                    CURRENT STATE                   EVENT 	                            NEXT STATE                           ACTION      */
		m_stateMachineSoupTCPZlib[      IDLE           ][      EVT_START        ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,      &SoupTCPZlib::StartSocketControl    );

		m_stateMachineSoupTCPZlib[ WAIT_TCP_CONNECTION ][     EVT_CONNECTED     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupTCPZlib::SendLoginRequestInitial   );
		m_stateMachineSoupTCPZlib[ WAIT_TCP_CONNECTION ][     EVT_STOP          ] = st_STATE_MACHINE(  WAIT_SOCKET_IDLE  ,    &SoupTCPZlib::StopSocketControl  );

		m_stateMachineSoupTCPZlib[     WAIT_LOGIN      ][   EVT_LOGIN_ACCEPTED  ] = st_STATE_MACHINE(  LOGGED_ON         ,    &SoupTCPZlib::ReadyToReceive            );
		m_stateMachineSoupTCPZlib[     WAIT_LOGIN      ][   EVT_LOGIN_REJECTED  ] = st_STATE_MACHINE(  WAIT_TO_RECONNECT ,    &SoupTCPZlib::WaitToReconnect           );
		m_stateMachineSoupTCPZlib[     WAIT_LOGIN      ][     EVT_STOP          ] = st_STATE_MACHINE(   WAIT_SOCKET_IDLE ,    &SoupTCPZlib::StopSocketControl  );
		m_stateMachineSoupTCPZlib[     WAIT_LOGIN      ][    EVT_TOUT_LOGIN     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupTCPZlib::SendLoginRequest          );
		m_stateMachineSoupTCPZlib[     WAIT_LOGIN      ][   EVT_SOCKET_ERROR    ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,     &SoupTCPZlib::DestroyTimerLogin  );

		m_stateMachineSoupTCPZlib[   WAIT_TO_RECONNECT ][    EVT_TOUT_LOGIN     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupTCPZlib::SendLoginRequest          );
		m_stateMachineSoupTCPZlib[   WAIT_TO_RECONNECT ][     EVT_STOP          ] = st_STATE_MACHINE(   WAIT_SOCKET_IDLE ,    &SoupTCPZlib::StopSocketControl  );

		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][    EVT_HB_SENDTOUT    ] = st_STATE_MACHINE(     LOGGED_ON      ,       &SoupTCPZlib::SendHeartbeat          );
		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][    EVT_HB_RECVTOUT    ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,      &SoupTCPZlib::Disconnect              );
		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][ EVT_SOCKET_DATA_PACKET] = st_STATE_MACHINE(     LOGGED_ON      ,      &SoupTCPZlib::ReceiveMessage          );
		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][   EVT_END_OF_SESSION  ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupTCPZlib::StopAndWait            );
		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][    EVT_SOCKET_ERROR   ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupTCPZlib::DestroyTimersHB        );
		m_stateMachineSoupTCPZlib[     LOGGED_ON       ][    EVT_STOP           ] = st_STATE_MACHINE( WAIT_SOCKET_IDLE   ,       &SoupTCPZlib::StopSocketControl  );


		m_stateMachineSoupTCPZlib[   WAIT_SOCKET_IDLE  ][    EVT_SOCKET_IDLE    ] = st_STATE_MACHINE(        IDLE        ,       &SoupTCPZlib::NotifyStop         );

		m_currentState = (int)IDLE;
		m_event = (int)SOUPTCPZLIB::NO_EVENT;
	}

	bool SoupTCPZlib::DoWait()
	{
		bool bRet = false;		
			
		if ( b_loopWait )
		{
			boost::xtime_get(&m_waitInterval, boost::TIME_UTC);
			m_waitInterval.nsec += 1*1000; // 1 us
			
			boost::thread::sleep(m_waitInterval);
		}

		if ( m_intEventSoup == 1 )
			bRet = true;

		return bRet;
	}

	void SoupTCPZlib::MainThreadLoop()
	{
		int actionEvent = SOCKETCONTROL::NO_EVENT;
		
		while ( !DoWait())
		{
			m_state = m_currentState;

			if (m_event != SOUPTCPZLIB::NO_EVENT)
			{
				if (m_stateMachineSoupTCPZlib[m_currentState][m_event].i_Proximo_Estado != SOUPTCPZLIB::REMAIN)
				{
					m_currentState = m_stateMachineSoupTCPZlib[m_currentState][ m_event].i_Proximo_Estado;

					DisplayState();
				}
				actionEvent = m_event;

				m_event = SOUPTCPZLIB::NO_EVENT;

				(this->*m_stateMachineSoupTCPZlib[m_state][actionEvent].acao)();
				
			}

			MonitorEvents();
		}
		
		Logger::logtrc('C',"SoupBinTCP::MainThreadLoop Finish");		
	}

	void SoupTCPZlib::DisplayState()
	{
		switch ( m_currentState )
		{	
		case IDLE:
			Logger::logtrc('C',"[SOUPTCPZLIB] STATE IDLE");
			break;
		case WAIT_TCP_CONNECTION:
			Logger::logtrc('C',"[SOUPTCPZLIB] STATE WAIT_TCP_CONNECTION");
			break;
		case WAIT_LOGIN:
			Logger::logtrc('C',"[SOUPTCPZLIB] STATE WAIT_LOGIN");
			break;
		case WAIT_SOCKET_IDLE:
			Logger::logtrc('C',"[SOUPTCPZLIB] STATE WAIT_SOCKET_IDLE");
			break;
		case WAIT_TO_RECONNECT:
			Logger::logtrc('C',"[SOUPTCPZLIB] STATE WAIT_TO_RECONNECT");
			break;
		case LOGGED_ON:
			//Logger::logtrc('C',"[SOUPTCPZLIB] STATE LOGGED_ON");
			break;

		}
		
	}

	void SoupTCPZlib::MonitorEvents()
	{
		//m_loopInterval = SOUPTCPZLIB::THREAD_LOOP_INTERVAL;
		b_loopWait = true;

		if ( m_IncomingMessageQueue.size() > 0 )
		{
			int iEvent = GetInEvent();
			//if ( !strEvent.compare("EVT_CONNECTED"))
			if ( iEvent == _CONNECTED_ )
			{
				m_event = EVT_CONNECTED;
				Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_CONNECTED");
			} 
			//else if ( !strEvent.compare("EVT_SOCKET_ERROR"))
			else if ( iEvent == _SOCKET_ERROR_ )
			{
				m_event = EVT_SOCKET_ERROR;
				Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_SOCKET_ERROR");
			}
			//else if ( !strEvent.compare("EVT_SOCKET_DATA"))
			else if ( iEvent == _SOCKET_DATA_ )
			{
				//m_loopInterval = 0;
				b_loopWait = false;
				ReceiveMessage();				
			} 
			else if ( iEvent == _ON_IDLE_ )
			//else if ( !strEvent.compare("EVT_ON_IDLE"))
			{
				m_event = EVT_SOCKET_IDLE;
				Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_ON_IDLE");
			} 			
		}


	}	

	int SoupTCPZlib::GetLastEvent()
	{
		int iEvent = _NO_EVENT_;
		
		if ( !m_IncomingMessageQueue.empty() )
			iEvent = m_IncomingMessageQueue[0];
			//iEvent = m_IncomingMessageQueue.back();

		return iEvent;
	}

	/// <summary>
	///	PUSH FRONT
	/// </summary>
	void SoupTCPZlib::SetOutEvent(int iEvent)
	{	
		boost::mutex::scoped_lock lockKeeper(m_socketControlPtr->m_IncommingQueueLock);

		m_socketControlPtr->m_IncomingMessageQueue.push_front(iEvent);
	}

	/// <summary>
	///	GET BACK
	/// </summary>
	int SoupTCPZlib::GetInEvent()
	{
		int iEvent = _NO_EVENT_;

		boost::mutex::scoped_lock lockKeeper(m_IncommingQueueLock);

		iEvent = m_IncomingMessageQueue.back();
		m_IncomingMessageQueue.pop_back();

		return iEvent;
	}


	void SoupTCPZlib::nop()
	{

	}

	void SoupTCPZlib::StartSocketControl()
	{
		Logger::logtrc('C',"[SOUPTCPZLIB] StartSocketControl()");
		m_socketControlPtr->Start();		
	}

	void SoupTCPZlib::StopSocketControl()
	{
		m_socketControlPtr->Stop();		
		Logger::logtrc('C',"[SOUPTCPZLIB] StopSocketControl()");
	}

	/// <summary>
	/// Send LOGIN REQUEST with SessionID blank and Requested Sequence Number equal 1.	
	/// </summary>
	void SoupTCPZlib::SendLoginRequestInitial()
	{
		Logger::logtrc('C',"[SOUPTCPZLIB] SendLoginRequestInitial()");

		timer_login.Create();
		timer_login.start();

		m_SoupTcpZlibMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		
		if ( m_socketControlPtr->Send( (unsigned char *)(&m_LoginRequestMsg), sizeof(MSG_SOUPTCP_LOGIN_REQUEST) ) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_SOCKET_ERROR");
		}
		
	}

	/// <summary>
	/// Send LOGIN REQUEST with current SessionID Requested Sequence with the last received sequence number.	
	/// </summary>
	void SoupTCPZlib::SendLoginRequest()
	{
		timer_login.start();

		// Fill LOGIN REQUEST SEQUENCE NUMBER

		if ( m_socketControlPtr->Send( (unsigned char *)&m_LoginRequestMsg, sizeof(MSG_SOUPTCP_LOGIN_REQUEST) ) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_SOCKET_ERROR");
		}
	}


	/// <summary>
	/// Saves the received SessionID and start a timer for Heartbeat.
	/// </summary>
	void SoupTCPZlib::ReadyToReceive()
	{
		Logger::logtrc('C',"[SOUPTCPZLIB] ReadyToReceive() ");

		timer_login.stop();
		timer_login.Destroy();

		
		mstr_sessionID.assign(m_LoginAccepted.alfa_Session,sizeof(m_LoginAccepted.alfa_Session));		
		mstr_msgSeqNum.assign(m_LoginAccepted.num_SeqNum , sizeof(m_LoginAccepted.num_SeqNum));		

		timer_send.Create();
		timer_send.start();	

		timer_recv.Create();
		timer_recv.start();		
	}	

	void SoupTCPZlib::ReceiveMessage()
	{
		unsigned long iAvailableBytes = m_socketControlPtr->GetSockAvailableBytes();

		if ( iAvailableBytes > 0 )
		{
			unsigned char * p_buffeRecv = (unsigned char *)malloc(iAvailableBytes);
			unsigned char * p_decompBuffer = (unsigned char *)malloc(20*iAvailableBytes);

			if ( !( (p_buffeRecv == NULL) || (p_decompBuffer == NULL) ) )
			{
				if ( m_socketControlPtr->Receive((unsigned char *)p_buffeRecv,iAvailableBytes) == iAvailableBytes )
				{

					if ( ZlibDecompress( p_buffeRecv ,  p_decompBuffer , iAvailableBytes, (20*iAvailableBytes)) == Z_OK )
					{
						for ( int index = 0 ; index < iAvailableBytes ; index++ )
						{
							if (m_SoupTcpZlibMsgState == 0)
							{
								if ( m_receiveIndex > SOUPTCPZLIB::MAX_BUFFER_SIZE )
								{
									Logger::logerr('C',"SoupTCPZlib::ReceiveMessage() - Buffer overflow! LineFeed caracter not found.");
									m_event = EVT_SOCKET_ERROR;
									free(p_decompBuffer);
									free(p_buffeRecv);

									return;
								}

								if ( p_decompBuffer[index] != 0x0A )
								{
									m_receiveBuffer[m_receiveIndex] = p_decompBuffer[index];
									m_receiveIndex++;
								} else
									m_SoupTcpZlibMsgState = 1;
							} else if( m_SoupTcpZlibMsgState == 1 ) {
								ParseMessage();
								m_SoupTcpZlibMsgState = 0;
								m_receiveIndex = 0;
							}
						}
					} else {
						Logger::logerr('C',"SoupTCPZlib::ReceiveMessage() - Error inflating received data through zlib.");
						m_event = EVT_SOCKET_ERROR;
					}
				}

				free(p_decompBuffer);
				free(p_buffeRecv);

			} else {
				Logger::logerr('C',"[SOUPTCPZLIB] Error at memory allocation at ReceiveMessage.");
				m_event = EVT_SOCKET_ERROR;
			}
		
			timer_recv.start();
		}
		
		

		

	}

	/// <summary>
	/// Parses the received data and sends ITCH message buffer to UMDFITCH2FIXConv .
	/// </summary>
	void SoupTCPZlib::ParseMessage()
	{
		char chMessageType = m_receiveBuffer[0];

		if ( chMessageType == SOUPTCPZLIB::LOGIN_ACCEPTED )
		{
			memcpy((char *)&m_LoginAccepted, m_receiveBuffer,sizeof(MSG_SOUPTCP_LOGIN_ACCEPTED));
			m_event = EVT_LOGIN_ACCEPTED;
			Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_LOGIN_ACCEPTED ");
		} 
		else if ( chMessageType == SOUPTCPZLIB::LOGIN_REJECTED ) 
		{
			m_event = EVT_LOGIN_REJECTED;
			Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_LOGIN_REJECTED ");
		}
		else if ( chMessageType == SOUPTCPZLIB::SEQUENCED_DATA_PACKET )
		{
			int msgSize = m_receiveIndex;			
			
			m_SequencedDataPacket.p_Message = (char *)malloc(msgSize - 1);
			memset(m_SequencedDataPacket.p_Message,' ',sizeof(m_SequencedDataPacket.p_Message));
			
			memcpy((char *)&m_SequencedDataPacket, m_receiveBuffer, 1);			
			memcpy((char *)m_SequencedDataPacket.p_Message, (char *)&m_receiveBuffer[1],msgSize - 1);
			
			PARENT->SendToTibco( (unsigned char *)m_SequencedDataPacket.p_Message, msgSize - 1);
			free(m_SequencedDataPacket.p_Message);
			
			IncrementSequenceNumber();

			// SEND SEQUENCE NUMBER TO TIBCO QUEUE => DELETE + NEW
		}
	}

	void SoupTCPZlib::IncrementSequenceNumber()
	{
		
		int sequence;
		char szSequence[15];
		
		sequence = atoi(mstr_msgSeqNum.c_str());
		sequence++;
		memset(szSequence,0x00,sizeof(szSequence));
		sprintf(szSequence,"%d",sequence);
		mstr_msgSeqNum = szSequence;
		
	}

	void SoupTCPZlib::WaitToReconnect()
	{		
		timer_login.start();		

		Logger::logtrc('C',"[SOUPTCPZLIB] WaitToReconnect()");
	}


	void SoupTCPZlib::SendHeartbeat()
	{
		MSG_SOUPTCP_HEARTBEAT_CLIENT heartbeat;

		static int pos = 0;
		if ( pos == 0 )
		{
			Logger::logtrcr('C',"[SOUPTCPZLIB] |");
			pos = 1;
		} else if ( pos == 1 ) {
			Logger::logtrcr('C',"[SOUPTCPZLIB] /");
			pos = 2;
		} else if ( pos == 2 ) {
			Logger::logtrcr('C',"[SOUPTCPZLIB] -");
			pos = 3;
		} else if ( pos == 3 ) {
			Logger::logtrcr('C',"[SOUPTCPZLIB] \\");
			pos = 0;
		} 
		//Logger::logtrc('C',"[SOUPTCPZLIB] SendHeartbeat()");
	
		if ( m_socketControlPtr->Send( (unsigned char *)&heartbeat, sizeof(MSG_SOUPTCP_HEARTBEAT_CLIENT)) == -1 )
		{
			m_event = EVT_SOCKET_ERROR;
			Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_SOCKET_ERROR");
		}
		else
			timer_send.start();		

	}

	/// <summary>
	/// Sends EVT_STOP to Event Bus
	/// </summary>
	void SoupTCPZlib::StopAndWait()
	{
		//SetOutEvent("EVT_STOP");
		SetOutEvent(_STOP_);

		timer_recv.Destroy();
		timer_send.Destroy();

		Logger::logtrc('C',"[SOUPTCPZLIB] StopAndWait()");
	}

	void SoupTCPZlib::TimerEventCallback(int tid)
	{
		if ( m_event != EVT_STOP)
		{
			switch ( tid )
			{
			case TID_HB_SEND_SOUPTCP:
				m_event = EVT_HB_SENDTOUT;
				//Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_HB_SENDTOUT");
				break;
			case TID_HB_RECV_SOUPTCP:
				m_event = EVT_HB_RECVTOUT;
				Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_HB_RECVTOUT");
				break;
			case TID_LOGIN_SOUPTCP:
				Logger::logtrc('C',"[SOUPTCPZLIB] EVENT EVT_TOUT_LOGIN");
				m_event = EVT_TOUT_LOGIN;
				break;
			}
		}
	}

	void SoupTCPZlib::Disconnect()
	{
		timer_recv.Destroy();
		timer_send.Destroy();
		
		//SetOutEvent("EVT_DISCONNECT");		
		SetOutEvent(_DISCONNECT_);		

		Logger::logtrc('C',"[SOUPTCPZLIB] Disconnect()");
	}

	void SoupTCPZlib::DestroyTimersHB()
	{
		timer_recv.Destroy();
		timer_send.Destroy();
	}

	void SoupTCPZlib::DestroyTimerLogin()
	{
		timer_login.stop();
		timer_login.Destroy();
	}

	void SoupTCPZlib::NotifyStop()
	{		
		Logger::logtrc('C',"SoupTCPZlib::NotifyStop()");

		m_conditionStop.notify_all();
	}

	int SoupTCPZlib::ZlibDecompress(unsigned char * p_in , unsigned char * p_out , int in_size, int out_size)
	{
		int ret;
		unsigned have;		

		
		do 
		{
			m_strm.avail_in = in_size;
			m_strm.next_in = p_in;
			
			do 
			{
				m_strm.avail_out = out_size;
				m_strm.next_out = p_out;

				ret = inflate(&m_strm, Z_NO_FLUSH);
				switch (ret) 
				{
				case Z_NEED_DICT:
					ret = Z_DATA_ERROR;     /* and fall through */
				case Z_DATA_ERROR:
				case Z_MEM_ERROR:
					(void)inflateEnd(&m_strm);
					return ret;
				}

				have = out_size - m_strm.avail_out;
				
				if ( have < 0 )
				{
					(void)inflateEnd(&m_strm);
					return Z_ERRNO;
				}			
			} while (m_strm.avail_out == 0);
			
		} while (ret != Z_STREAM_END);

		return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
	}

	string SoupTCPZlib::GetLastSequenceNumber()
	{
		return mstr_msgSeqNum;
	}

	void SoupTCPZlib::ZeroReceiveBuffer()
	{
		m_receiveIndex = 0;
		memset( m_receiveBuffer, 0x00 , sizeof(m_receiveBuffer) );
	}

}