#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

namespace NasdaqProxy  
{
	const int _NO_EVENT_		= -1;
	const int _SOCKET_DATA_		= 0;
	const int _STOP_			= 1;
	const int _DISCONNECT_		= 2;
	const int _SOCKET_ERROR_	= 3;
	const int _CONNECTED_		= 4;
	const int _ON_IDLE_			= 5;

	const int _LOG_DATA_		= 6;
	const int _LOG_CONTROL_		= 7;
	const int _LOG_ERROS_		= 8;
	const int _LOG_TRACE_		= 9;
}

#endif