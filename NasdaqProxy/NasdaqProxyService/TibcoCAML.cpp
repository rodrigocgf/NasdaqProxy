/*

=========================================================


				NASDAQ PROXY SERVICE

	Module	: TibcoCAML.cpp


=========================================================

	Author	: Rodrigo C. G. Fran�a
	Company : 7COMm / Consulting Group
	version : 1.0
	Date	: june 9 , 2010
=========================================================

*/

#include "StdAfx.h"
#include <tchar.h>
#include "TibcoCAML.h"
#include "./NasdaqParser/Logger.h"
//#include "bmfwin32/Dbg.h"
//#include "BmfWin32/ILog.h"

namespace NasdaqProxy 
{ 


	CTibcoCAML::CTibcoCAML(void) : m_pTransport(NULL) , m_pReceiver(NULL) , m_pTopic(NULL) , m_pSender(NULL)
	{
	}

	CTibcoCAML::~CTibcoCAML(void)
	{
	}

	
	void CAML_C_API CTibcoCAML::TaggedMessageCallback(TaggedMessage* pMessage, void* pClosure)
	{

	}
	
	bool CTibcoCAML::Send(unsigned char * buffer, int size)
	{
		unsigned char * szBuffer = (unsigned char *)malloc(size + 1);
		memset(szBuffer,0x00, (size + 1) );

		memcpy(szBuffer,buffer,size);

		ResultCode rc = CAML_OK;

		m_pMessage->setLabel("ToRendezvous");
		m_pMessage->setFieldValue(1, (char *)szBuffer);

		rc = m_pSender->send(m_pMessage);
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Failure To Send Message: %i, Vendor Error: %i", rc, CAML_VENDOR_ERROR(rc));
			return false;
		} 

		m_pMessage->reset();

		free(szBuffer);
		return true;
	}

	bool CTibcoCAML::Start(char * szConfigFile)
	{
		ResultCode rc = CAML_OK;
	
		ConfigurationContext* pCfgCtx = 0;
		
		
		if( strlen(szConfigFile) == 0 ) 
		{
			Logger::logerr('C',"[TIBCO]usage: AsynchReceiver <path to xml configuration file>");			
			return false;
		} 

		rc = ContextFactory::createConfigurationContext(&pCfgCtx, szConfigFile);
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error Creating Configuration Contexted: %i", rc);
			return false;
		} 

		rc = CAML::configure(pCfgCtx);
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error Configuring CAML Libraries: %i", rc);
			return false;
		} 

		rc = CAML::createTransport(&m_pTransport, "TEST_TPORT1");
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error Creating Transport: %i", rc);
			return false;
		} 

		rc = CAML::createTopic(&m_pTopic, "TEST_TOPIC");
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error createing Topic: %i", rc);
			return false;
		}

		return true;
	}

	bool CTibcoCAML::StartSender()
	{
		ResultCode rc = CAML_OK;

		rc = m_pTransport->createAsynchSender(m_pTopic, &m_pSender, "TEST_SEND");
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error Creating Asynchronous Sender: %i", rc);
			return false;
		} 

		rc = m_pTransport->createMessage(&m_pMessage);
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error Creating Message: %i", rc);
			return false;
		} 

		return true;
	}

	bool CTibcoCAML::StartThrReceiver()
	{
		ResultCode rc = CAML_OK;

		rc = m_pTransport->createAsynchReceiver(m_pTopic, &m_pReceiver, "TEST_RECV", &CTibcoCAML::TaggedMessageCallback, this);
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error creating The Asynchronous Receiver: %i", rc);
			return false;
		} 

		rc = m_pReceiver->start();
		if(rc != CAML_OK) 
		{
			Logger::logerr('C',"[TIBCO]Error starting The Asynchronous Receiver: %i", rc);
			return false;
		}

		return true;
	}
	
}